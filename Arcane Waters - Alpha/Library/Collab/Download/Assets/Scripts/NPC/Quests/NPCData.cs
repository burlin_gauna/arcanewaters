﻿using System.Collections.Generic;
using UnityEngine;

public class NPCData : ScriptableObject
{
   #region Public Variables

   // Holds the list of quests
   public List<NPCQuestData> npcQuestList;

   #endregion
}