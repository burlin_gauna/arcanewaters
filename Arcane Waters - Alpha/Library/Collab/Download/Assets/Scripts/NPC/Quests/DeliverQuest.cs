﻿using System.Collections.Generic;
using UnityEngine;

public class DeliverQuest : ScriptableObject
{
   #region Public Variables

   // Item data to be delivered
   public Item itemToDeliver;

   // Queantity of items to be delivered
   public int quantity;

   #endregion Public Variables
}