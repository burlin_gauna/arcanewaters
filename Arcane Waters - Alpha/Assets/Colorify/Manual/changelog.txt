1.7 (Unity 5.6+)
- ColorifyToTexture static class for possibility to use Colorify to recolor Texture2D into RenderTexture from scripts
- ColorifyTextureUpdater - ColorifyToTexture MonoBehaviour wrapper 

1.6b (Unity 5.6+)
 - compatibility changes to standard shader versions for Unity 5.6

1.6 (Unity 5.5+)
- compatibility changes to standard shader versions for Unity 5.5
- added "bake texture" options for baking recolored textures into file
- Linear colorspace handling
-- Disable default sRGB conversion on masks
-- sRGB corrections to baked textures
- minor fixes to mask creation process

1.5a:
- Added Playmaker support.

1.5:
- Added Unity 5-compatible Toon shaders
- Standard PBS shader:
-- Added backface and cull-off versions
-- Added luminosity recolor mode
-- Added color multipliers
-- Added specular recolor and smoothness control

1.4:
- Added Unity 5 standard shader variants
- Fixed unlit shaders with negative scale

1.3a:
- fixed warning messages

1.3:
- Added in-editor mask baking
- Added recolor mask family of shaders

1.2:
- Added Maskless dissolve shaders

1.1:
- Added Toon outline shaders
- Added Unlit shaders for sprite rendering (transparent, cutout and simple)

1.0b:
- unified the way main color is handled by different shader families
- small manual updates.