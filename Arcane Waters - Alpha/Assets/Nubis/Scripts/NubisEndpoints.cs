﻿public class NubisEndpoints
{
   #region "Public Variables"

   // RPC - Nubis Endpoint. Used for invoking methods.
   public const string RPC = "rpc";

   // Terminate - Nubis Endpoint. Used to stop Nubis' execution.
   public const string TERMINATE = "terminate";

   // Log - Nubis Endpoint. Used to access Nubis logs.
   public const string LOG = "log";

   // STATUS - Nubis Endpoint. Used to check Nubis' status.
   public const string STATUS = "status";

   // VERSION - Nubis Endpoint. Used to retrieve Nubis' version.
   public const string VERSION = "version";

   #endregion

}