﻿#if NUBIS
public enum NubisStatus
{
   Starting,
   Running,
   Stopping,
   Idle
}

#endif