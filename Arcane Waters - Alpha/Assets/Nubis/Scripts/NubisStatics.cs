﻿#if NUBIS
public class NubisStatics
{
   #region "Public Variables"

   // The name of the application.
   public const string APP_NAME = "Nubis";

   // Versioning
   public const string VERSION = "2020.08.26.2136";

   #endregion
}
#endif