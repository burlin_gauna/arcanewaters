using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Mirror;

// The elemental damage types
public enum Element {
   Physical = 0,
   Fire = 1,
   Earth = 2,
   Air = 3,
   Water = 4,
   Heal = 5,
   ALL = 6,
   Poison = 7
}