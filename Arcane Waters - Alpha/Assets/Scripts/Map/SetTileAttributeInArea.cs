﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Mirror;

public class SetTileAttributeInArea : MonoBehaviour
{
   #region Public Variables

   // Type of attribute
   public TileAttributes.Type type;

   // The area in which to set
   public Collider2D areaCollider;

   #endregion

   #region Private Variables

   #endregion
}
