﻿using System;
using UnityEngine;

[Serializable]
public class GenericBiomeSpritePair {
   // The biome type
   public Biome.Type biomeType;

   // The sprite
   public Sprite sprite;
}