﻿public class UserSearchResultCollection
{
   #region Public Variables

   // The search that generated this results collection
   public UserSearchInfo searchInfo;

   // The results
   public UserSearchResult[] results;

   #endregion
}
