﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Mirror;

public class EnemyBattleCollider : MonoBehaviour {
   #region Public Variables

   // Reference to the enemy
   public Enemy enemy;

   // Collider component
   public CircleCollider2D battleCollider;

   #endregion

   #region Private Variables
      
   #endregion
}
