﻿public class RemoteSettingsManager
{
   public class SettingNames
   {
      #region Public Variables

      // The "Players Count Max" Setting
      public const string PLAYERS_COUNT_MAX = "PLAYERS_COUNT_MAX";

      // The "Admin-Only Mode" Setting
      public const string ADMIN_ONLY_MODE = "ADMIN_ONLY_MODE";

      // The "Admin-Only Mode Message" Setting
      public const string ADMIN_ONLY_MODE_MESSAGE = "ADMIN_ONLY_MODE_MESSAGE";

      // The "Sandbox mode switch for Steam purchases" Setting
      public const string STEAM_PURCHASES_SANDBOX_MODE_ACTIVE = "STEAM_PURCHASES_SANDBOX_MODE_ACTIVE";

      // The "Enable Gem Store" Setting
      public const string ENABLE_GEM_STORE = "ENABLE_GEM_STORE";

      // The "Gem Store Disabled Message"
      public const string GEM_STORE_DISABLED_MESSAGE = "GEM_STORE_DISABLED_MESSAGE";

      #endregion
   }
}
