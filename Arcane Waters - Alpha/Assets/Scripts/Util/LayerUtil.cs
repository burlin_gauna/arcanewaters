﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class LayerUtil : MonoBehaviour
{
   #region Public Variables

   // The names of commonly used layers
   public const string EVERYTHING = "Everything";
   public const string SEA_MONSTERS = "SeaMonsters";
   public const string SHIPS = "Ships";
   public const string GRID_COLLIDERS = "GridColliders";
   public const string SEA_STRUCTURES = "Enemy";
   public const string DEFAULT = "Default";
   public const string PLAYER_BIPEDS = "PlayerBipeds";

   #endregion


   #region Private Variables

   #endregion
}
