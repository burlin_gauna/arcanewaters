﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Mirror;

// The types of administration privileges
public enum PrivilegeType { None = 0, Admin = 1, QA = 2, ContentWriter = 3, Support = 4 }

//public static class AdminUtil {
//   #region Public Variables

//   #endregion

//   #region Private Variables

//   #endregion
//}
