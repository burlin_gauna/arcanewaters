﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Mirror;

// Determines the damage effetiveness
public enum DamageMagnitude
{
   Default = 0,
   Resistant = 1,
   Weakness = 2
}