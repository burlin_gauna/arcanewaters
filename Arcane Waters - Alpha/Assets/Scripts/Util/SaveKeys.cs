﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Mirror;

public class SaveKeys : MonoBehaviour {
   #region Public Variables

   // Our save keys
   public static string LAST_ACCOUNT_NAME = "last_account_name";
   public static string EFFECTS_ON = "effects_on";
   public static string MUSIC_ON = "music_on";
   public static string EFFECTS_VOLUME = "effects_volume";
   public static string MUSIC_VOLUME = "music_volume";

   #endregion

   #region Private Variables

   #endregion
}
