﻿public interface IBiomable {
   void setBiome (Biome.Type biomeType, bool skipClientOnlyFunctionality);
}
