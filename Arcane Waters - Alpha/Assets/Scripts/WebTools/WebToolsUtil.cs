﻿public static class WebToolsUtil
{
   public enum ActionSource
   {
      None = 0,
      Game = 1,
      WebTools = 2,
      Email = 3
   }

   public enum SupportTicketType
   {
      None = 0,
      Complaint = 1,
      Help = 2,
      SuspiciousActivity = 3
   }
}
