﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TMPro;

public class AutoTyper : MonoBehaviour {
   #region Public Variables
   // The default amount of delay per character
   public static float CHAR_DELAY = .03f;

   // The default amount of delay per word
   public static float WORD_DELAY = .18f;

   // The amount of delay between beeps while the text is typing
   public static float BEEP_DELAY = .07f;

   #endregion

   public static void typeText (Text textElement, string text) {
      typeText(textElement, text, CHAR_DELAY, true);
   }

   public static void typeText (Text textElement, string text, bool usePlaceholderSpaces) {
      typeText(textElement, text, CHAR_DELAY, usePlaceholderSpaces);
   }

   public static void typeText (Text textElement, string text, float characterDelay, bool usePlaceholderSpaces) {
      textElement.StopAllCoroutines();
      textElement.StartCoroutine(setText(textElement, text, characterDelay, usePlaceholderSpaces));
   }

   public static void typeText (TextMeshProUGUI textElement, string text, bool usePlaceholderSpaces) {
      typeText(textElement, text, CHAR_DELAY, usePlaceholderSpaces);
   }

   public static void typeText (TextMeshProUGUI textElement, string text, float characterDelay, bool usePlaceholderSpaces) {
      textElement.StopAllCoroutines();
      textElement.StartCoroutine(setText(textElement, text, characterDelay, usePlaceholderSpaces));
   }

   public static void typeWords (Text textElement, string text) {
      textElement.StopAllCoroutines();
      textElement.StartCoroutine(setWords(textElement, text, WORD_DELAY));
   }

   public static void finishText (Text textElement, string text) {
      textElement.StopAllCoroutines();
      textElement.text = text;
   }

   public static void finishText (TextMeshProUGUI text) {
      text.GetComponent<RollingTextFade>().finishFading();
   }

   public static void slowlyRevealText (Text textElement, string text) {
      textElement.StopAllCoroutines();
      textElement.text = CLEAR_START + text + CLEAR_END;

      // Slowly move the CLEAR_START forward in the text
      textElement.StartCoroutine(moveColorClearForward(textElement));
   }

   public static void slowlyRevealText (TextMeshProUGUI text, string msg) {
      text.GetComponent<RollingTextFade>().fadeInText(msg);
   }

   protected static IEnumerator setText (Text textElement, string text, float characterDelay, bool usePlaceholderSpaces) {
      float lastBeepTime = 0f;
      textElement.text = "";

      // Set all of the non-space character to non-breaking spaces initially
      if (usePlaceholderSpaces) {
         for (int i = 0; i < text.Length; i++) {
            if (text[i] != ' ') {
               textElement.text += '\u00A0';
            } else {
               textElement.text += " ";
            }
         }
      }

      for (int i = 0; i < text.Length; i++) {
         float startTime = Time.realtimeSinceStartup;
         string initialText = textElement.text;
         textElement.text = text.Substring(0, i + 1);

         // Play some little beeps while the text is being shown
         if (text[i] != ' ' && text[i] != '\n') {
            if (Time.realtimeSinceStartup - lastBeepTime > BEEP_DELAY) {
               // SoundManager.play2DClip(SoundManager.Type.Blip_2, SoundManager.BEEP_VOLUME);
               lastBeepTime = Time.realtimeSinceStartup;
            }
         }

         // If we haven't reached the last character, then append the non-breaking spaces
         if (i < text.Length - 1 && usePlaceholderSpaces) {
            textElement.text += initialText.Substring(i + 1);
         }

         while (Time.realtimeSinceStartup - startTime < characterDelay) {
            yield return null;
         }
      }
   }

   protected static IEnumerator setText (TextMeshProUGUI textElement, string text, float characterDelay, bool usePlaceholderSpaces) {
      float lastBeepTime = 0f;
      textElement.text = "";

      // Set all of the non-space character to non-breaking spaces initially
      if (usePlaceholderSpaces) {
         for (int i = 0; i < text.Length; i++) {
            if (text[i] != ' ') {
               textElement.text += '\u00A0';
            } else {
               textElement.text += " ";
            }
         }
      }

      for (int i = 0; i < text.Length; i++) {
         float startTime = Time.realtimeSinceStartup;
         string initialText = textElement.text;

         // In case this is an item insert tag, we want to advance it all fully at once
         int insertIndex = text.IndexOf("<link=\"" + ChatManager.ITEM_INSERT_ID_PREFIX, i);
         int insertEndIndex = text.IndexOf("</link>", i);
         if (insertIndex != -1 && insertEndIndex != -1 && insertIndex == i && insertIndex < insertEndIndex) {
            i = insertEndIndex + 6;
         }

         textElement.text = text.Substring(0, i + 1);

         // Play some little beeps while the text is being shown
         if (text[i] != ' ' && text[i] != '\n') {
            if (Time.realtimeSinceStartup - lastBeepTime > BEEP_DELAY) {
               // SoundManager.play2DClip(SoundManager.Type.Blip_2, SoundManager.BEEP_VOLUME);
               lastBeepTime = Time.realtimeSinceStartup;
            }
         }

         // If we haven't reached the last character, then append the non-breaking spaces
         if (i < text.Length - 1 && usePlaceholderSpaces) {
            textElement.text += initialText.Substring(i + 1);
         }

         while (Time.realtimeSinceStartup - startTime < characterDelay) {
            yield return null;
         }
      }
   }

   protected static IEnumerator setWords (Text textElement, string text, float wordDelay) {
      textElement.text = "";
      string[] words = text.Split(' ');

      for (int i = 0; i < words.Length; i++) {
         float startTime = Time.realtimeSinceStartup;
         textElement.text += words[i] + " ";

         while (Time.realtimeSinceStartup - startTime < wordDelay) {
            yield return null;
         }
      }
   }

   protected static IEnumerator moveColorClearForward (Text textElement) {
      yield return new WaitForSeconds(CHAR_DELAY);

      string msg = textElement.text;

      // Check the index of the opening color tag
      int index = msg.IndexOf(CLEAR_START);

      // Remove the opening and closing color tags for now
      msg = msg.Replace(CLEAR_START, "");
      msg = msg.Replace(CLEAR_END, "");

      // If there's still room left, keep moving the color tag forward
      if (index < msg.Length) {
         msg = msg.Insert(index + 1, CLEAR_START);
         msg += CLEAR_END;

         // Update the text
         textElement.text = msg;

         // SFX
         //SoundManager.play2DClip(SoundManager.Type.Blip_2);
         //SoundEffectManager.self.playFmodSfx(SoundEffectManager.CLICK_TAB);

         // Keep moving forward
         textElement.StartCoroutine(moveColorClearForward(textElement));
      }
   }

   #region Private Variables

   // The color tag that begins making the letters clear (invisible)
   protected static string CLEAR_START = "<COLOR='#0000'>";

   // The color tag that stops making the letters clear (invisible)
   protected static string CLEAR_END = "</COLOR>";

   #endregion
}