﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Mirror;
using System;
using TMPro;
using System.Linq;

public class Crop : ClientMonoBehaviour {
   #region Public Variables

   // The type of Crop
   public enum Type { None = 0,
      Potatoes = 1, Onions = 2, Carrots = 3, Corn = 4, Garlic = 5,
      Lettuce = 6, Peas = 7, Chilies = 8, Radishes = 9, Tomatoes = 10,
   }

   // The type of Crop
   public Type cropType;

   // The user who owns the crop
   public int userId;

   // The spot number for this crop
   public int cropNumber;

   // The time the crop was planted
   public long creationTime;

   // The time the crop was last watered
   public long lastWaterTimestamp;

   // The amount of time we have to wait between watering
   public float waterInterval;

   // The current growth level of this crop
   public int growthLevel;

   // Our floating water icon
   public SpriteRenderer floatingWaterIcon;

   // Our floating harvest icon
   public SpriteRenderer floatingHarvestIcon;

   // Our floating water level container
   public GameObject waterLevelContainer;

   // The bar that shows when this crop is ready for water
   public Image waterBar;

   // Our simple animation component
   [HideInInspector]
   public SimpleAnimation anim;

   // If the data is set
   public bool dataIsSet;

   // The Object Holder
   public GameObject objectHolder;

   // The key of the area that this crop is in
   public string areaKey;

   // Whether crop debugging is enabled or disabled
   private static bool debuggingEnabled = false;

   // A reference to the button that will toggle the debug information for this crop
   public GameObject debugButton;

   // A reference to the canvas showing debug information for this crop
   public GameObject debugCanvas;

   // References to the text on the debug canvas
   public TextMeshProUGUI cropTypeText, cropNumText, userIdText, growthLevelText, timeUntilWaterableText, areaKeyText;

   #endregion

   protected override void Awake () {
      base.Awake();

      // Look up components
      anim = GetComponent<SimpleAnimation>();
      debugButton.SetActive(debuggingEnabled);
      debugCanvas.SetActive(false);
   }

   public void hideCrop () {
      objectHolder.SetActive(false);
      GetComponent<SpriteRenderer>().enabled = false;
   }

   public void setData (Crop.Type cropType, int spotNumber, long lastWaterTimeStamp, string areaKey) {
      this.lastWaterTimestamp = lastWaterTimeStamp;
      this.cropType = cropType;
      this.cropNumber = spotNumber;
      this.areaKey = areaKey;
      dataIsSet = true;
   }

   private void Update () {
      if (!dataIsSet || !TimeManager.self.hasReceivedInitialData) {
         return;
      }

      float currentWaterAlpha = floatingWaterIcon.color.a;
      float currentHarvestAlpha = floatingHarvestIcon.color.a;

      // If we're ready to be watered, fade in a water icon
      Util.setAlpha(floatingWaterIcon, isReadyForWater() ?
         currentWaterAlpha + Time.smoothDeltaTime : currentWaterAlpha - Time.smoothDeltaTime);

      // If we're ready to be harvested, fade in a harvest icon
      Util.setAlpha(floatingHarvestIcon, isMaxLevel() ?
         currentHarvestAlpha + Time.smoothDeltaTime : currentHarvestAlpha - Time.smoothDeltaTime);

      // Update our water level display
      updateWaterLevelDisplay();
      updateDebugCanvas();
   }

   public override bool Equals (object rhs) {
      if (rhs is Crop) {
         var other = rhs as Crop;
         return cropType == other.cropType && userId == other.userId && cropNumber == other.cropNumber && creationTime == other.creationTime;
      }
      return false;
   }

   public override int GetHashCode () {
      unchecked // Overflow is fine, just wrap
      {
         int hash = 17;
         hash = hash * 23 + cropType.GetHashCode();
         hash = hash * 23 + userId.GetHashCode();
         hash = hash * 23 + cropNumber.GetHashCode();
         hash = hash * 23 + creationTime.GetHashCode();
         return hash;
      }
   }

   public int getMaxGrowthLevel () {
      return getMaxGrowthLevel(this.cropType);
   }

   public static int getMaxGrowthLevel (Crop.Type cropType) {
      CropsData newCropData = CropsDataManager.self.getCropData(cropType);
      return newCropData.maxGrowthLevel;
   }

   public bool isMaxLevel () {
      return (this.growthLevel >= getMaxGrowthLevel());
   }

   public bool isReadyForWater (bool logWaterCropData = false) {
      if (isMaxLevel()) {
         return false;
      }

      bool isPlantReadyToBeWatered = getTimeSinceWatered() > this.waterInterval;

      if (logWaterCropData && isPlantReadyToBeWatered) {
         D.adminLog("Plant is ready to be watered: {" 
            + TimeManager.self.getLastServerUnixTimestamp() 
            + " - " + lastWaterTimestamp
            + " = " + getTimeSinceWatered()
            + "} > {" + this.waterInterval + "}", D.ADMIN_LOG_TYPE.Crop);
      }

      return isPlantReadyToBeWatered;
   }

   public static int getXP (Crop.Type cropType) {
      CropsData cropData = CropsDataManager.self.getCropData(cropType);
      return cropData.rewardXp;
   }

   protected void updateWaterLevelDisplay () {
      // There are several situations where we don't need to show the water display at all
      if (Global.player == null || this.isMaxLevel() || this.isReadyForWater() || !isPlayerNear() || !isPlayerHoldingWaterCan()) {
         waterLevelContainer.SetActive(false);
         return;
      }

      // Make sure the container shows
      waterLevelContainer.SetActive(true);

      // Check how close we are to being ready for water
      float waterReadiness = getTimeSinceWatered() / (this.waterInterval);

      // Update the water bar size
      waterBar.fillAmount = waterReadiness;
   }

   protected float getTimeSinceWatered () {
      float perkMultiplier = PerkManager.self.getPerkMultiplier(Perk.Category.CropGrowthSpeed);
      float timeSinceWatered = (TimeManager.self.getLastServerUnixTimestamp() - lastWaterTimestamp);
      return timeSinceWatered * perkMultiplier;
   }

   protected bool isPlayerNear () {
      if (Global.player == null) {
         return false;
      }

      return Vector2.Distance(Global.player.transform.position, this.transform.position) < 3f;
   }

   protected bool isPlayerHoldingWaterCan () {
      if (Global.player != null && Global.player is BodyEntity) {
         BodyEntity playerBody = (BodyEntity) Global.player;

         return (playerBody.weaponManager.actionType == Weapon.ActionType.WaterCrop);
      }

      return false;
   }

   public bool hasBeenHarvested () {
      return !objectHolder.activeSelf;
   }

   public CropInfo getCropInfo () {
      CropInfo cropInfo = new CropInfo(cropType, userId, cropNumber, creationTime, lastWaterTimestamp, (int)waterInterval, growthLevel, areaKey);
      return cropInfo;
   }

   public void toggleDebugCanvas () {
      debugCanvas.SetActive(!debugCanvas.activeSelf);
   }

   private void setDebugButton () {
      debugButton.SetActive(debuggingEnabled);
   }

   private void updateDebugCanvas () {
      if (!debugCanvas.activeInHierarchy) {
         return;
      }

      cropTypeText.text = cropType.ToString();
      cropNumText.text = "CropNum: " + cropNumber.ToString();
      userIdText.text = "UserId: " + userId.ToString();
      growthLevelText.text = "Growth: " + growthLevel.ToString();

      float timeUntilWaterable = Mathf.Clamp(waterInterval - getTimeSinceWatered(), 0.0f, float.MaxValue);
      timeUntilWaterableText.text = "Waterable in: " + timeUntilWaterable.ToString("F2");
      areaKeyText.text = areaKey;
   }

   public static void setCropDebugging (bool value) {
      if (debuggingEnabled == value) {
         return;
      }

      debuggingEnabled = value;

      List<Crop> crops = FindObjectsOfType<Crop>().ToList();
      foreach (Crop crop in crops) {
         crop.setDebugButton();

         if (!value) {
            crop.debugCanvas.SetActive(false);
         }
      }
   }

   #region Private Variables

   #endregion
}
