﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Text;
using MapCreationTool.Serialization;
using MapCustomization;
using System.Threading.Tasks;
using Steam;
using Store;
using Rewards;

public class DB_MainStub : MonoBehaviour
{
   #region Public Variables

   #endregion

   #region Nubis Requests

   public static string fetchSingleBlueprint (int bpId, int usrId) {
      return "";
   }

   public static string fetchZipRawData () {
      return "";
   }

   public static string userInventory (int usrId, Item.Category[] categoryFilter, int[] itemIdsToExclude,
      bool mustExcludeEquippedItems, int currentPage, int itemsPerPage, Item.DurabilityFilter itemDurabilityFilter) {
      return "";
   }

   public static int userInventoryCount (int usrId, Item.Category[] categoryFilter, int[] itemIdsToExclude,
      bool mustExcludeEquippedItems, Item.DurabilityFilter itemDurabilityFilter) {
      return 0;
   }

   public static string fetchXmlVersion () {
      return "";
   }

   public static string fetchCraftableRings (int usrId) {
      return "";
   }

   public static string fetchCraftableNecklace (int usrId) {
      return "";
   }

   public static string fetchCraftableTrinkets (int usrId) {
      return "";
   }

   public static string fetchCraftableHats (int usrId) {
      return "";
   }

   public static string fetchCraftableIngredients (int usrId) {
      return "";
   }

   public static string fetchCraftableArmors (int usrId) {
      return "";
   }

   public static string fetchCraftableWeapons (int usrId) {
      return "";
   }

   public static string fetchCraftingIngredients (int usrId) {
      return "";
   }

   public static string fetchCategorizedItem (int usrId, int categorizedItem) {
      return "";
   }

   public static string fetchEquippedItems (int usrId) {
      return "";
   }

   public static string fetchMapData (string mapName, int version) {
      return "";
   }

   public static List<int> getUserWithNoAbilities (List<int> abilityIds) {
      return new List<int>();
   }

   public static List<AbilitySQLData> userAbilities (int usrId, AbilityEquipStatus abilityEquipStatus) {
      return new List<AbilitySQLData>();
   }

   #endregion

   public static List<QuestTimer> getQuestTimers () {
      return new List<QuestTimer>();
   }

   public static string getMapContents () {
      return "";
   }

   public static List<XMLPair> getPvpShopXML () {
      return new List<XMLPair>();
   }

   public static TreasureStateData getTreasureStateForChest (int userId, int chestId, string areaId) {
      return new TreasureStateData();
   }

   public static int updateTreasureStatus (int userId, int treasureId, string areaKey) {
      return 0;
   }

   public static List<AuctionItemData> getAuctionListString (string pageNumber, string rowsPerPage, string categoryFilter, string userId, string auctionFilter) {
      return new List<AuctionItemData>();
   }

   public static List<AuctionItemData> getAuctionList (int pageNumber, int rowsPerPage, Item.Category[] categoryFilter, int userId, AuctionPanel.ListFilter auctionFilter) {
      return new List<AuctionItemData>();
   }

   public static int getAuctionListCount (int userId, Item.Category[] filterData, AuctionPanel.ListFilter auctionFilter) {
      return 0;
   }

   public static List<AuctionItemData> getAuctionsToDeliver () {
      return null;
   }

   public static void deliverAuction (int auctionId, int mailId, int recipientUserId, string mailMessageOverride, string mailSenderNameOverride) {

   }

   public static void deleteAuction (int auctionId) {

   }

   public static int createAuction (int sellerUserId, string sellerName, int mailId, DateTime expiryDate, bool isBuyoutAllowed, int highestBidPrice, int buyoutPrice, Item.Category itemCategory, string itemName, int itemCount) {
      return 0;
   }

   public static void updateAuction (int auctionId, int highestBidUser, int highestBidPrice, DateTime expiryDate) {

   }

   public static AuctionItemData getAuction (int auctionId, bool readItemData) {
      return null;
   }

   public static void addBidderOnAuction (int auctionId, int userId, int bidAmount) {

   }

   public static List<AuctionManager.BidderData> getBidders (int auctionId) {
      return new List<AuctionManager.BidderData>();
   }

   public static void updateNPCQuestXML (string rawData, int typeIndex, string xmlName, int isActive) {
   }

   public static List<XMLPair> getNPCQuestXML () {
      return new List<XMLPair>();
   }

   public static void deleteNPCQuestXML (int typeID) {
   }

   public static List<XMLPair> getProjectileXML () {
      return new List<XMLPair>();
   }

   public static void updateBiomeTreasureDrops (int xmlId, string rawXmlContent, Biome.Type biomeType) {
   }

   public static List<XMLPair> getBiomeTreasureDrops () {
      return new List<XMLPair>();
   }

   public static void writeZipData (byte[] bytes, int slot) {
   }

   public static string getXmlContent (string tableName, EditorSQLManager.EditorToolType toolType = EditorSQLManager.EditorToolType.None) {
      return "";
   }

   public static List<RawPaletteToolData> getPaletteXmlContent (string tableName) {
      return new List<RawPaletteToolData>();
   }

   public static int getLatestXmlVersion () {
      return 0;
   }

   public static string getLastUpdate (EditorSQLManager.EditorToolType editorType) {
      return "";
   }

   public static ChatInfo getLatestChatInfo () {
      return new ChatInfo();
   }

   public static void updateCompanionExp (int xmlId, int userId, int exp) {
   }

   public static void updateCompanions (int xmlId, int userId, string companionName, int companionLevel, int companionType, int equippedSlot, string iconPath, int companionExp) {

   }

   public static void updateCompanionRoster (int xmlId, int userId, int slot) {

   }

   public static List<CompanionInfo> getCompanions (int userId) {
      return new List<CompanionInfo>();
   }

   public static void updateAccountMode (int accoundId, bool isSinglePlayer) {

   }

   public static void updateCropsXML (string rawData, int xmlId, int cropsType, bool isEnabled, string cropsName) {
   }

   public static List<XMLPair> getCropsXML () {
      return new List<XMLPair>();
   }

   public static void deleteCropsXML (int xmlId) {
   }

   public static int updateBackgroundXML (int xmlId, string rawData, string bgName) {
      return 0;
   }

   public static List<XMLPair> getBackgroundXML () {
      return new List<XMLPair>();
   }

   public static void deleteBackgroundXML (int xmlId) {
   }

   public static List<SQLEntryNameClass> getSQLDataByName (EditorSQLManager.EditorToolType editorType) {
      return new List<SQLEntryNameClass>();
   }

   public static List<SQLEntryIDClass> getSQLDataByID (EditorSQLManager.EditorToolType editorType, EquipmentType equipmentType = EquipmentType.None) {
      return new List<SQLEntryIDClass>();
   }

   public static void updateShipAbilities (int shipId, string abilityXML) {

   }

   public static void updateShipAbilityXML (string rawData, string shipAbilityName, int xmlId) {
   }

   public static List<XMLPair> getShipAbilityXML () {
      return new List<XMLPair>();
   }

   public static void deleteShipAbilityXML (int xmlId) {
   }

   public static void updateEquipmentXML (string rawData, int typeID, EquipmentType equipType, string equipmentName, bool isEnabled) {

   }

   public static void deleteEquipmentXML (int type, EquipmentType equipType) {
   }

   public static List<XMLPair> getQuestItemtXML () {
      return new List<XMLPair>();
   }

   public static List<XMLPair> getEquipmentXML (EquipmentType equipType) {
      return new List<XMLPair>();
   }

   public static List<ItemDefinition> getItemDefinitions () {
      return new List<ItemDefinition>();
   }

   public static void createNewItemDefinition (ItemDefinition definition) {

   }

   public static void updateItemDefinition (ItemDefinition definition) {

   }

   public static void deleteItemDefinition (int id) {

   }

   #region Haircuts XML

   public static void updateHaircutXML (int xmlID, string rawData, int accIDOverride = 0) {
      return;
   }

   public static List<XMLPair> getHaircutsXML () {
      return new List<XMLPair>();
   }

   #endregion

   #region Dyes XML

   public static void updateDyeXML (int xmlID, string rawData, int accIDOverride = 0) {
      return;
   }

   public static bool toggleDyeXML (int xmlID, bool isEnabled, int accIDOverride = 0) {
      return false;
   }

   public static List<XMLPair> getDyesXML () {
      return new List<XMLPair>();
   }

   #endregion

   #region Ship Skins XML

   public static void updateShipSkinXML (int xmlID, string rawData, int accIDOverride = 0) {
      return;
   }

   public static List<XMLPair> getShipSkinsXML () {
      return new List<XMLPair>();
   }

   #endregion

   #region Gems XML

   public static void updateGemsXML (int xmlID, string rawData, int accIDOverride = 0) {
      return;
   }

   public static List<XMLPair> getGemsXML () {
      return new List<XMLPair>();
   }

   #endregion

   #region Consummables XML

   public static void updateConsumableXML (int xmlID, string rawData, int accIDOverride = 0) {
      return;
   }

   public static List<XMLPair> getConsumableXML () {
      return new List<XMLPair>();
   }

   #endregion

   public static void updateCraftingXML (int xmlID, string rawData, string name, int typeId, int category) {

   }

   public static void deleteCraftingXML (int xmlID) {

   }

   public static string getTooltipXmlContent () {
      return "";
   }

   public static List<TooltipSqlData> getTooltipData () {
      return new List<TooltipSqlData>();
   }

   public static List<XMLPair> getCraftingXML () {
      return new List<XMLPair>();
   }

   public static void updateAchievementXML (string rawData, string name, int xmlId) {

   }

   public static void deleteAchievementXML (string name) {
   }

   public static List<XMLPair> getAchievementXML () {
      return new List<XMLPair>();
   }

   #region Perks XML

   public static void updatePerksXML (string xmlContent, int perkId) {
   }

   public static List<PerkData> getPerksXML () {
      return new List<PerkData>();
   }

   public static void deletePerkXML (int xmlId) {
   }

   #endregion

   #region Perks

   public static bool resetPerkPointsAll (int usrId, int perkPoints) {
      return false;
   }

   public static List<Perk> getPerkPointsForUser (int usrId) {
      return new List<Perk>();
   }

   public static void assignPerkPoint (int usrId, int perkId) {
   }

   public static int getUnassignedPerkPoints (int usrId) {
      return 0;
   }

   public static int getAssignedPointsByPerkId (int usrId, int perkId) {
      return 0;
   }

   public static bool addPerkPointsForUser (int usrId, int perkId, int perkPoints) {
      return false;
   }

   public static void addPerkPointsForUser (int usrId, List<Perk> perks) {
   }

   #endregion

   public static void upsertBook (string content, string title, int bookId) {
   }

   public static void deleteBookByID (int bookId) {
   }

   public static BookData getBookById (int bookId) {
      return new BookData();
   }

   public static List<BookData> getBooksList () {
      return new List<BookData>();
   }

   public static void duplicateDiscovery (DiscoveryData data) {

   }

   public static void upsertDiscovery (DiscoveryData data) {

   }

   public static DiscoveryData getDiscoveryById (int discoveryId) {
      return new DiscoveryData();
   }

   public static void deleteDiscoveryById (int discoveryId) {

   }

   public static List<DiscoveryData> getDiscoveriesList () {
      return new List<DiscoveryData>();
   }

   public static UserDiscovery getUserDiscovery (int userId, int placedDiscoveryId) {
      return null;
   }

   public static List<UserDiscovery> getUserDiscoveries (int userId) {
      return new List<UserDiscovery>();
   }

   public static void setUserDiscovery (UserDiscovery discovery) {
   }

   public static void completeStepForUser (int userId, int stepId) {
   }

   public static void completeStepForUser (int userId, string actionCode) {
   }

   public static bool userHasCompletedAction (int userId, string actionCode) {
      return false;
   }

   public static void updateNPCXML (string rawData, int typeIndex) {

   }

   public static List<string> getNPCXML () {
      return new List<string>();
   }

   public static void deleteNPCXML (int typeID) {

   }

   public static void setCustomHouseBase (object command, int userId, int baseMapId) {

   }

   public static void setCustomFarmBase (object command, int userId, int baseMapId) {

   }

   public static void setCustomGuildMapBase (object command, int guildId, int baseMapId) {

   }

   public static void setCustomGuildHouseBase (object command, int guildId, int baseMapId) {

   }

   public static int getCustomHouseBaseId (int userId) {
      return 0;
   }

   public static int getCustomFarmBaseId (int userId) {
      return 0;
   }

   public static int getCustomGuildMapBaseId (int guildId) {
      return 0;
   }

   public static int getCustomGuildHouseBaseId (int guildId) {
      return 0;
   }

   public static void updateGuildName (int gldId, string newGuildName) {

   }

   public static MapCustomizationData getMapCustomizationData (object command, int mapId, int userId) {
      return null;
   }

   public static PrefabState getMapCustomizationChanges (object command, int mapId, int userId, int prefabId) {
      return default;
   }

   public static void setMapCustomizationChanges (object command, int mapId, int userId, PrefabState changes) {

   }

   public static int getMapId (string areaKey) {
      return -1;
   }

   public static List<Map> getMaps (object command) {
      return new List<Map>();
   }

   public static List<Map> getMaps () {
      return new List<Map>();
   }

   public static string getMapInfo (string areaKey) {
      return null;
   }

   public static Dictionary<string, MapInfo> getLiveMaps () {
      return new Dictionary<string, MapInfo>();
   }

   public static List<MapVersion> getMapVersions (Map map) {
      return new List<MapVersion>();
   }

   public static string getMapLiveVersionGameData (string mapName) {
      return null;
   }

   public static string getMapVersionEditorData (MapVersion version) {
      return null;
   }

   public static MapVersion getLatestMapVersionEditor (Map map, bool infiniteCommandTimeout = false) {
      return null;
   }

   public static List<MapSpawn> getMapSpawnsById (int mapId) {
      return new List<MapSpawn>();
   }

   public static List<MapSpawn> getMapSpawns () {
      return new List<MapSpawn>();
   }

   public static void createMap (MapVersion mapVersion, string comment) {

   }

   public static void duplicateMapGroup (int mapId, int newCreatorId) {

   }

   public static void updateMapDetails (Map map) {

   }

   public static MapVersion createNewMapVersion (MapVersion mapVersion, Biome.Type biome, int creatorUserId, string comment) {
      return null;
   }

   public static void updateMapVersion (MapVersion mapVersion, Biome.Type biomeType, MapCreationTool.EditorType editorType, int creatorUserId, string comment, bool infiniteCommandTimeout = false) {

   }

   public static void deleteMap (int id) {

   }

   public static void deleteMapGroup (int mapId) {

   }

   public static void deleteMapVersion (MapVersion version) {

   }

   public static void publishLatestVersionForAllGroup (int mapId) {

   }

   public static PublishedVersionChange setLiveMapVersion (MapVersion version) {
      return new PublishedVersionChange();
   }

   protected static void insertMapChangeComment (int mapId, int mapVersion, int userId, string comment) {

   }

   public static List<MapChangeComment> getVersionChangeComments (int mapId, int mapVersion) {
      return new List<MapChangeComment>();
   }

   protected static void noteMapVersionPublish (int mapId) {

   }

   public static void updateShopXML (string rawData, string shopName, int xmlId) {

   }

   public static List<XMLPair> getShopXML () {
      return new List<XMLPair>();
   }

   public static void deleteShopXML (int xmlId) {

   }

   public static void updateShipXML (string rawData, int typeIndex, Ship.Type shipType, string shipName, bool isActive) {

   }

   public static List<XMLPair> getShipXML () {
      return new List<XMLPair>();
   }

   public static void deleteShipXML (int typeID) {

   }

   public static void updateSeaMonsterXML (string rawData, int typeIndex, SeaMonsterEntity.Type enemyType, string battlerName, bool isActive) {

   }

   public static void deleteSeamonsterXML (int typeID) {
   }

   public static List<XMLPair> getSeaMonsterXML () {
      return new List<XMLPair>();
   }

   public static List<XMLPair> getLandMonsterXML () {
      return new List<XMLPair>();
   }

   public static void deleteLandmonsterXML (int typeID) {
   }

   public static void updateLandMonsterXML (string rawData, int typeIndex, Enemy.Type enemyType, string battlerName, bool isActive) {

   }

   public static void updateAbilitiesData (int userID, AbilitySQLData abilityData) {

   }

   public static bool hasAbility (int userId, int abilityId) {
      return false;
   }

   public static void updateAbilitySlot (int userID, int abilityId, int slotNumber) {

   }

   public static List<AbilityXMLContent> getDefaultAbilities () {
      return new List<AbilityXMLContent>();
   }

   public static List<SoundEffect> getSoundEffects () {
      return new List<SoundEffect>();
   }

   public static void updateSoundEffect (SoundEffect effect) {

   }

   public static void deleteSoundEffect (SoundEffect effect) {

   }

   public static void updateAchievementData (AchievementData achievementData, int userID, bool hasReached, int addedCount) {

   }

   public static List<AchievementData> getAchievementData (int userID, ActionType actionType) {
      return new List<AchievementData>();
   }

   public static List<AchievementData> getAchievementDataList (int userID) {
      return new List<AchievementData>();
   }

   public static void updateBattleAbilities (int skillId, string abilityName, string abilityXML, int abilityType) {

   }

   public static void deleteBattleAbilityXML (int skillId) {

   }

   public static List<AbilityXMLContent> getBattleAbilityXML () {
      return null;
   }

   public static void readTest () {

   }

   public static void createNPCRelationship (int npcId, int userId, int friendshipLevel) {

   }

   public static bool hasFriendshipLevel (int npcId, int userId) {
      return false;
   }

   public static int getFriendshipLevel (int npcId, int userId) {
      return 0;
   }

   public static void updateNPCRelationship (int npcId, int userId, int friendshipLevel) {

   }

   public static void createQuestStatus (int npcId, int userId, int questId, int questNodeId) {

   }

   public static void resetQuestsWithNodeId (int questId, int questNodeId) {
   }

   public static List<Item> getRequiredItems (List<Item> itemList, int userId) {
      return new List<Item>();
   }

   public static void updateQuestStatus (int npcId, int userId, int questId, int questNodeId, int dialogueId) {

   }

   public static QuestStatusInfo getQuestStatus (int npcId, int userId, int questId, int questNodeId) {
      return null;
   }

   public static List<QuestStatusInfo> getQuestStatuses (int npcId, int userId) {
      return null;
   }

   public static List<Item> getRequiredIngredients (int usrId, List<CraftingIngredients.Type> itemList) {
      return new List<Item>();
   }

   public static List<CropInfo> getCropInfo (int userId) {
      return null;
   }

   public static List<CropInfo> getGuildCropInfo (int guildId) {
      return null;
   }

   public static int insertCrop (CropInfo cropInfo, string areaKey) {
      return 0;
   }

   public static void waterCrop (CropInfo cropInfo) {

   }

   public static List<PenaltyInfo> getPenaltiesForAccount (int accId) {
      return new List<PenaltyInfo>();
   }

   public static PenaltyInfo getPenaltyForAccount (int accId, List<PenaltyInfo.ActionType> penaltyTypes) {
      return null;
   }

   public static bool savePenalty (PenaltyInfo penalty) {
      return false;
   }

   public static int getAccountId (string accountName, string accountPassword, string accountPasswordCapsLock) {
      return -1;
   }

   public static int getOverriddenAccountId (string accountName) {
      return -1;
   }

   public static int getSteamAccountId (string accountName) {
      return -1;
   }

   public static string getAccountName (int userId) {
      return "";
   }

   public static Tuple<int, int, string> getUserDataTuple (string username) {
      return Tuple.Create(0, 0, "");
   }

   public static int getAccountPermissionLevel (int accountId) {
      return 0;
   }

   public static List<UserInfo> getUsersForAccount (int accId, int userId = 0) {
      return new List<UserInfo>();
   }

   public static List<int> getAssociatedUserIdsWithSteamId (ulong steamId) {
      return new List<int>();
   }

   public static List<UserInfo> getDeletedUsersForAccount (int accId, int userId = 0) {
      return new List<UserInfo>();
   }

   public static List<PlantableTreeDefinition> getPlantableTreeDefinitions (object command) {
      return new List<PlantableTreeDefinition>();
   }

   public static List<PlantableTreeInstanceData> getPlantableTreeInstances (object command, string areaKey) {
      return new List<PlantableTreeInstanceData>();
   }

   public static PlantableTreeInstanceData getPlantableTreeInstance (object command, int id) {
      return null;
   }

   public static void createPlantableTreeInstance (object command, PlantableTreeInstanceData instance) {

   }

   public static void updatePlantableTreeInstance (object command, PlantableTreeInstanceData instance) {

   }

   public static void deletePlantableTreeInstance (object command, int id) {

   }

   public static List<Armor> getArmorForAccount (int accId, int userId = 0) {
      return null;
   }

   public static List<Weapon> getWeaponsForAccount (int accId, int userId = 0) {
      return null;
   }

   public static List<Hat> getHatsForAccount (int accId, int userId = 0) {
      return new List<Hat>();
   }

   public static List<Weapon> getWeaponsForUser (int userId) {
      return null;
   }

   public static List<Armor> getArmorForUser (int userId) {
      return null;
   }

   public static List<Hat> getHatsForUser (int userId) {
      return null;
   }

   public static void setWeaponId (int userId, int newWeaponId) {

   }

   public static void deleteCrop (int cropId, int userId, string areaKey) {

   }

   public static void setHatId (int userId, int newHatId) {

   }


   public static void setArmorId (int userId, int newArmorId) {

   }

   public static void setRingId (int userId, int newId) {

   }

   public static void setNecklaceId (int userId, int newId) {

   }


   public static void setTrinketId (int userId, int newId) {

   }

   public static bool tradeItemsIfValid (int userId1, List<Item> items1, int gold1, int userId2, List<Item> items2, int gold2) {
      return false;
   }

   public static bool hasItem (int userId, int itemId, int itemCategory) {
      return false;
   }

   public static void setNewLocalPosition (int userId, Vector2 localPosition, Direction facingDirection, string areaKey) {

   }

   public static void setNewLocalPositionWhenInArea (List<int> userIds, List<string> areaKeys, Vector2 localPosition, Direction facingDirection, string areaKey) {

   }

   public static void storeShipHealthAndFood (int shipId, float shipHealthPercentage, int shipFood) {

   }

   public static void restoreShipMaxHealthAndFood (int shipId) {
   }

   public static int getUserId (string username) {
      return 0;
   }

   public static int getDeletedUserId (string username) {
      return 0;
   }

   public static bool addGold (int userId, int amount) {
      return false;
   }

   public static void addGoldAndXP (int userId, int gold, int XP) {

   }

   public static int storeChatLog (int userId, string userName, string message, DateTime dateTime, ChatInfo.Type chatType, string ipAddress, string extra) {
      return 0;
   }

   public static List<ChatInfo> getChat (ChatInfo.Type chatType, int seconds, string ipAddress, bool hasInterval = true, int limit = 0) {
      return new List<ChatInfo>();
   }

   public static int getAccountId (int userId) {
      return 0;
   }

   public static int getAccountStatus (int accountId) {
      return 0;
   }

   public static string getUserInfoJSON (int userId) {
      return "";
   }

   public static UserInfo getUserInfoById (int userId) {
      return null;
   }

   public static Dictionary<int, UserInfo> getUserInfosByIds (IEnumerable<int> userIds) {
      return new Dictionary<int, UserInfo>();
   }

   public static UserInfo getUserInfo (string userName) {
      return null;
   }

   public static UserAccountInfo getUserAccountInfo (string username) {
      return null;
   }

   public static List<PenaltiesQueueItem> getPenaltiesQueue () {
      return new List<PenaltiesQueueItem>();
   }

   public static void removePenaltiesQueueItem (int id) {

   }

   public static List<QueueItem> getUserNamesChangesQueue () {
      return new List<QueueItem>();
   }

   public static void processUserNameChangeFromQueue (int id, bool isValid) {

   }

   public static List<UserInfo> getUserInfoList (int[] userIdArray) {
      return null;
   }

   public static List<int> getAllUserIds () {
      return null;
   }

   public static ShipInfo getShipInfo (int userId) {
      return null;
   }

   public static ShipInfo getShipInfoForUser (int userId) {
      return null;
   }

   public static Armor getArmor (int userId) {
      return null;
   }

   public static Weapon getWeapon (int userId) {
      return null;
   }

   public static bool setArmorPalette (int userId, int armorId, string armorPalettes) {
      return false;
   }

   public static bool setHatPalette (int userId, int hatId, string hatPalettes) {
      return false;
   }

   public static bool setWeaponPalette (int userId, int weaponId, string weaponPalettes) {
      return false;
   }

   public static UserObjects getUserObjects (int userId) {
      return null;
   }

   public static UserObjects getUserObjectsForDeletedUser (int userId) {
      return null;
   }

   public static string getUserName (int userId) {
      return "";
   }

   public static void changeUserPvpState (int userId, int pvpState) {

   }

   public static void changeUserName (NameChangeInfo info) {

   }

   public static void changeDeletedUserName (NameChangeInfo info) {

   }

   public static int createUser (int accountId, int usrAdminFlag, UserInfo userInfo, Area area) {
      return 0;
   }

   public static Item createNewItem (int userId, Item baseItem) {
      return null;
   }

   public static int insertNewArmor (int userId, int armorType, string palettes) {
      return 0;
   }

   public static int insertNewWeapon (int userId, int weaponType, string palettes, int count = 1) {
      return 0;
   }

   public static void setItemOwner (int userId, int itemId) {

   }

   public static void deleteAllFromTable (int accountId, int userId, string table) {

   }

   public static void deleteUser (int accountId, int userId) {

   }

   public static void deactivateUser (int accountId, int userId) {

   }

   public static void activateUser (int accountId, int userId) {

   }

   public static void forgetUser (int accountId, int userId) {

   }

   public static void forgetUserBySpot (int accountId, int charSpot) {

   }

   public static void insertTrackedUserActions (List<TrackedUserAction> actions) {
   }

   public static bool doesUserExists (int userId) {
      return false;
   }

   public static RemoteSetting getRemoteSetting (string settingName) {
      return null;
   }

   public static bool setRemoteSetting (string rsName, string rsValue, RemoteSetting.RemoteSettingValueType rsValueType) {
      return false;
   }

   public static RemoteSettingCollection getRemoteSettings (string[] settingNames) {
      return null;
   }

   public static bool setRemoteSettings (RemoteSettingCollection collection) {
      return false;
   }

   public static ShipInfo createStartingShip (int userId) {
      return null;
   }

   public static ShipInfo createShipFromShipyard (int userId, ShipInfo shipyardInfo) {
      return null;
   }

   public static int deleteShipForUser (int usrId, int shpId) {
      return -1;
   }

   public static void setCurrentShip (int userId, int shipId) {

   }

   public static int getGold (int userId) {
      return 0;
   }

   public static int getGems (int accountId) {
      return 0;
   }

   public static int getItemID (int userId, int itmCategory, int itmType) {
      return 0;
   }

   public static void createOrUpdateItemListCount (int userId, List<Item> itemList) {

   }

   public static Item createItemOrUpdateItemCount (int userId, Item baseItem) {
      return null;
   }

   public static void transferItem (Item item, int fromUserId, int toUserId, int amount) {

   }

   public static void updateItemQuantity (int userId, int itmId, int itmCount) {

   }

   public static void updateItemDurability (int userId, int itemId, int durability) {

   }

   public static int getItemDurability (int userId, int itemId) {
      return 0;
   }

   public static bool decreaseQuantityOrDeleteItem (int userId, Item.Category itemCategory, int itemTypeId, int deductedValue) {
      return false;
   }

   public static bool decreaseQuantityOrDeleteItem (int userId, int itmId, int deductCount) {
      return false;
   }

   public static int getItemCountByType (int userId, int itemCategory, int itemType) {
      return 0;
   }

   public static List<ItemTypeCount> getItemTypeCounts (int userId) {
      return new List<ItemTypeCount>();
   }

   public static int getItemCountByCategory (int userId, Item.Category[] categories) {
      return 0;
   }

   public static int getItemCount (int userId, Item.Category[] categories, int[] itemIdsToFilter, Item.Category[] categoriesToFilter) {
      return 0;
   }

   public static List<Item> getItems (int userId, Item.Category[] categories, int page, int itemsPerPage) {
      return null;
   }

   public static List<Item> getItems (int userId, Item.Category[] categories, int page, int itemsPerPage,
      List<int> itemIdsToFilter, List<Item.Category> categoriesToFilter) {
      return null;
   }

   public static List<Item> getItems (int userId, Item.Category[] categories, int page, int itemsPerPage,
      List<int> itemIdsToFilter) {
      return null;
   }

   public static List<Item> getCraftingIngredients (int usrId, List<CraftingIngredients.Type> ingredientTypes) {
      return null;
   }

   public static Item getItem (int userId, int itemId) {
      return null;
   }

   public static Item getItem (int itemId) {
      return null;
   }

   public static Item getFirstItem (int userId, Item.Category itemCategory, int itemTypeId) {
      return null;
   }

   public static void updateItemShortcut (int userId, int slotNumber, int itemId) {

   }

   public static void deleteItemShortcut (int userId, int slotNumber) {

   }

   public static List<ItemShortcutInfo> getItemShortcutList (int userId) {
      return null;
   }

   public static void addGems (int accountId, int amount) {

   }

   public static int getXP (int userId) {
      return 0;
   }

   public static int insertNewUsableItem (int userId, UsableItem.Type itemType, string palettes) {
      return 0;
   }

   public static int insertNewUsableItem (int userId, UsableItem.Type itemType, Ship.SkinType skinType) {
      return 0;
   }

   public static int insertNewUsableItem (int userId, UsableItem.Type itemType, HairLayer.Type hairType) {
      return 0;
   }

   public static void setHairColor (int userId, string newPalette) {

   }

   public static void setHairType (int userId, HairLayer.Type newType) {

   }

   public static void setShipSkin (int shipId, Ship.SkinType newSkin) {

   }

   public static int deleteItem (int userId, int itemId) {
      return 0;
   }

   public static Stats getStats (int userId) {
      return null;
   }

   public static int setGuildInventoryIfNotExists (int guildId, int inventoryId) {
      return default;
   }

   public static Jobs getJobXP (int userId) {
      return null;
   }

   public static string getJobXPString (int userId) {
      return "";
   }

   public static bool isGuildAlly (int guildId, int allyId) {
      return false;
   }

   public static GuildInfo getGuildInfo (int guildId) {
      return null;
   }

   public static List<int> getGuildAlliance (int guildId) {
      return new List<int>();
   }

   public static int addGuildAlliance (int guildId, int allyId) {
      return -1;
   }

   public static int removeGuildAlliance (int guildId, int allyId) {
      return -1;
   }

   public static string getGuildInfoJSON (int guildId) {
      return null;
   }

   public static List<UserInfo> getUsersForGuild (int guildId) {
      return new List<UserInfo>();
   }

   public static int getUserGuildId (int userId) {
      return -1;
   }

   public static int getMemberCountForGuild (int guildId) {
      return 0;
   }

   public static int createGuild (GuildInfo guildInfo) {
      return 0;
   }

   public static void deleteGuild (int guildId) {

   }

   public static void deleteGuildRanks (int guildId) {

   }

   public static void deleteGuildRank (int guildId, int rankId) {

   }

   public static void assignGuild (int userId, int guildId) {

   }

   public static void assignRankGuild (int userId, int guildRankId) {

   }

   public static int getLowestRankIdGuild (int guildId) {
      return -1;
   }

   public static void createRankGuild (GuildRankInfo rankInfo) {

   }

   public static void updateRankGuild (GuildRankInfo rankInfo) {

   }

   public static void updateRankGuildByID (GuildRankInfo rankInfo, int id) {

   }

   public static int getGuildLeader (int guildId) {
      return -1;
   }

   public static List<GuildRankInfo> getGuildRankInfo (int guildId) {
      return null;
   }

   public static int getGuildMemberPermissions (int userId) {
      return 0;
   }

   public static int getGuildMemberRankId (int userId) {
      return -1;
   }

   public static int getGuildMemberRankPriority (int userId) {
      return -1;
   }

   public static void addJobXP (int userId, Jobs.Type jobType, int XP) {

   }

   public static void insertIntoJobs (int userId) {

   }

   public static List<ShipInfo> getShips (int userId, int page, int shipsPerPage) {
      return null;
   }

   public static void addToTradeHistory (int userId, TradeHistoryInfo tradeInfo) {

   }

   public static int getTradeHistoryCount (int userId) {
      return 0;
   }

   public static List<TradeHistoryInfo> getTradeHistory (int userId, int page, int tradesPerPage) {
      return null;
   }

   public static void pruneJobHistory (DateTime untilDate) {
   }

   public static List<LeaderBoardInfo> calculateLeaderBoard (Jobs.Type jobType,
      LeaderBoardsManager.Period period, DateTime startDate, DateTime endDate) {
      return null;
   }

   public static void deleteLeaderBoards (LeaderBoardsManager.Period period) {
   }

   public static void updateLeaderBoards (List<LeaderBoardInfo> entries) {
   }

   public static void updateLeaderBoardDates (LeaderBoardsManager.Period period,
      DateTime startDate, DateTime endDate) {
   }

   public static DateTime getLeaderBoardEndDate (LeaderBoardsManager.Period period) {
      return DateTime.UtcNow;
   }

   public static void getLeaderBoards (LeaderBoardsManager.Period period, out List<LeaderBoardInfo> farmingEntries,
      out List<LeaderBoardInfo> sailingEntries, out List<LeaderBoardInfo> exploringEntries, out List<LeaderBoardInfo> tradingEntries,
      out List<LeaderBoardInfo> craftingEntries, out List<LeaderBoardInfo> miningEntries, out List<LeaderBoardInfo> badgesEntries) {
      farmingEntries = null;
      sailingEntries = null;
      exploringEntries = null;
      tradingEntries = null;
      craftingEntries = null;
      miningEntries = null;
      badgesEntries = null;
   }

   public static void createFriendship (int userId, int friendUserId, Friendship.Status friendshipStatus, DateTime lastContactDate) {
   }

   public static void updateFriendship (int userId, int friendUserId, Friendship.Status friendshipStatus, DateTime lastContactDate) {
   }

   public static void deleteFriendship (int userId, int friendUserId) {
   }

   public static FriendshipInfo getFriendshipInfo (int userId, int friendUserId) {
      return null;
   }

   public static Dictionary<int, FriendshipInfo> getFriendshipInfos (int userId, IEnumerable<int> friendUserIds) {
      return new Dictionary<int, FriendshipInfo>();
   }

   public static List<FriendshipInfo> getFriendshipInfoList (int userId, Friendship.Status friendshipStatus, int page, int friendsPerPage) {
      return null;
   }

   public static int getFriendshipInfoCount (int userId, Friendship.Status friendshipStatus) {
      return 0;
   }

   public static List<int> getUserIdsHavingPendingFriendshipRequests (DateTime startDate) {
      return null;
   }

   public static CustomItemCollection createCustomItemCollection () {
      return default;
   }

   public static int createMail (MailInfo mailInfo) {
      return 0;
   }

   public static void updateMailReadStatus (int mailId, bool isRead) {
   }

   public static void deleteMail (int mailId) {
   }

   public static MailInfo getMailInfo (int mailId) {
      return null;
   }

   public static List<MailInfo> getMailInfoList (int userId, int page, int mailsPerPage) {
      return null;
   }

   public static List<MailInfo> getSentMailInfoList (int senderUsrId, int page, int mailsPerPage) {
      return null;
   }

   public static int getMailInfoCount (int userId) {
      return 0;
   }

   public static int getSentMailInfoCount (int userId) {
      return 0;
   }

   public static List<int> getUserIdsHavingUnreadMail (DateTime startDate) {
      return null;
   }

   public static bool hasUnreadMail (int userId) {
      return false;
   }

   public static int getMinimumClientGameVersionForWindows () {
      return 0;
   }

   public static int getUsrAdminFlag (int accountId) {
      return 0;
   }

   public static string[] getPlayersNames (int[] userIds) {
      return Array.Empty<string>();
   }

   public static int getMinimumClientGameVersionForMac () {
      return 0;
   }

   public static int getMinimumClientGameVersionForLinux () {
      return 0;
   }

   public static int getMinimumToolsVersionForWindows () {
      return 0;
   }

   public static int getMinimumToolsVersionForMac () {
      return 0;
   }

   public static int getMinimumToolsVersionForLinux () {
      return 0;
   }

   public static void setServerFromConfig () {

   }

   public static T exec<T> (Func<object, T> action) {
      return default;
   }

   public static void exec (Action<object> action) {

   }

   public static async Task<T> execAsync<T> (Func<object, T> action) {
      await Task.CompletedTask;
      return default;
   }

   public static async Task execAsync (Action<object> action) {
      await Task.CompletedTask;
   }

   public static bool updateDeploySchedule (long scheduleDateAsTicks, int buildVersion) {
      return false;
   }

   public static DeployScheduleInfo getDeploySchedule () {
      return null;
   }

   public static bool finishDeploySchedule () {
      return true;
   }

   public static bool cancelDeploySchedule () {
      return false;
   }

   public static void createServerHistoryEvent (DateTime eventDate, ServerHistoryInfo.EventType eventType, int serverVersion, int serverPort) {
   }

   public static List<ServerHistoryInfo> getServerHistoryList (int serverPort, long startDateBinary, int maxRows) {
      return new List<ServerHistoryInfo>();
   }

   public static List<int> getOnlineServerList () {
      return new List<int>();
   }

   public static void updateServerShutdown (bool shutdown) {

   }

   public static bool getServerShutdown () {
      return false;
   }

   public static bool isMasterServerOnline () {
      return false;
   }

   public static void pruneServerHistory (DateTime untilDate) {
   }

   public static bool setGameMetric (string machineId, string processId, string processName, string name, string value, Metric.MetricValueType valueType, string description = "") {
      return false;
   }

   public static MetricCollection getGameMetrics (string name) {
      return null;
   }

   public static void clearOldGameMetrics (int maxLifetimeSeconds = 300) {
   }

   public static int getTotalPlayersCount () {
      return 0;
   }

   protected static bool setMetric (string machineId, string serverAddress, string serverPort, string keySuffix, string value) {
      return false;
   }

   public static bool setMetricPlayersCount (string machineId, string serverAddress, string serverPort, int playerCount) {
      return false;
   }

   public static bool setMetricAreaInstancesCount (string machineId, string serverAddress, string serverPort, int areaInstancesCount) {
      return false;
   }

   public static bool setMetricPort (string machineId, string processName, string PID, int port) {
      return false;
   }

   public static bool setMetricIP (string machineId, string processName, string PID, string ip) {
      return false;
   }

   public static bool setMetricUptime (string machineId, string processName, string PID, long uptime) {
      return false;
   }

   public static void updatePaletteXML (string rawData, string name, int xmlId, int isEnabled, string tag) {

   }

   public static void deletePaletteXML (string name) {

   }

   public static void deletePaletteXML (int id) {

   }

   public static int getPaletteTagID (string tag) {
      return -1;
   }

   public static List<PaletteXMLPair> getPaletteXML (bool onlyEnabledPalettes) {
      return new List<PaletteXMLPair>();
   }

   public static List<PaletteXMLPair> getPaletteXML (int tagId) {
      return new List<PaletteXMLPair>();
   }

   public static List<PaletteXMLPair> getPaletteXML (int tagId, string subcategory) {
      return new List<PaletteXMLPair>();
   }

   public static int createAccount (string accountName, string accountPassword, string accountEmail, int validated) {
      return 0;
   }

   public static void saveSessionEvent (SessionEventInfo sessionEvent) {

   }

   //public static void storeGameAccountLoginEvent (int usrId, int accId, string usrName, string ipAddress, string machineIdent, int deploymentId) {

   //}

   //public static void storeGameUserCreateEvent (int usrId, int accId, string usrName, string ipAddress) {

   //}

   //public static void storeGameUserDestroyEvent (int usrId, int accId, string usrName, string ipAddress) {

   //}

   public static void setAdmin (int userId, int adminFlag) {

   }

   public static void addVisitedAreas (int userId, IEnumerable<string> areaKeys) {

   }

   public static List<string> getVisitedAreas (int userId) {
      return new List<string>();
   }

   public static bool hasUserVisitedArea (int userId, string areaKey) {
      return false;
   }

   public static int getUserCountHavingVisitedArea (string areaKey) {
      return 0;
   }

   public static void noteUserAreaVisit (int userId, string areaKey) {

   }

   public static List<string> getLastUserVisitedAreas (int userId) {
      return new List<string>();
   }

   public static void incrementWorldAreaVisitStreak (int userId) {

   }

   public static void resetWorldAreaVisitStreak (int userId) {

   }

   public static void addAdminGameSettings (AdminGameSettings settings) {
   }

   public static void updateAdminGameSettings (AdminGameSettings settings) {
   }

   public static AdminGameSettings getAdminGameSettings () {
      return null;
   }

   public static ulong createSteamOrder () {
      return 0;
   }

   public static bool deleteSteamOrder (ulong orderId) {
      return false;
   }

   public static bool toggleSteamOrder (ulong orderId, bool closed) {
      return false;
   }

   public static bool updateSteamOrder (ulong orderId, int userId, string status, string content) {
      return false;
   }

   public static SteamOrder getSteamOrder (ulong orderId) {
      return null;
   }

   public static ulong getLastSteamOrderId () {
      return 0;
   }

   public static ulong createStoreItem () {
      return 0;
   }

   public static bool updateStoreItem (ulong itemId, Item.Category category, int soldItemId, bool isEnabled, int price, string storeItemName, string storeItemDescription) {
      return false;
   }

   public static bool deleteStoreItem (ulong itemId) {
      return false;
   }

   public static StoreItem getStoreItem (ulong itemId) {
      return null;
   }

   public static List<StoreItem> getAllStoreItems () {
      return new List<StoreItem>();
   }

   public static Haircut getHaircut (int hcId) {
      return null;
   }

   public static List<Haircut> getAllHaircuts () {
      return new List<Haircut>();
   }

   public static int insertNewHaircut (int userId, int haircutId, HairLayer.Type hairType) {
      return 0;
   }

   public static Item fetchHaircut (int userId, HairLayer.Type hairType) {
      return null;
   }

   public static int insertNewShipSkin (int userId, int shipSkinId, Ship.Type shipType, Ship.SkinType skinType) {
      return 0;
   }

   public static void clearWorldMap () {
      return;
   }

   public static bool uploadWorldMapSector (byte[] data) {
      return false;
   }

   public static int getWorldMapSectorsCount () {
      return 0;
   }

   public static List<XMLPair> getLandPowerupXML () {
      return new List<XMLPair>();
   }

   #region Soul Binding

   public static Item.SoulBindingType getSoulBindingType (Item.Category itemCategory, int itemTypeId) {
      return Item.SoulBindingType.None;
   }

   public static List<ItemTypeSoulbinding> getAllSoulBindingTypeInfo () {
      return new List<ItemTypeSoulbinding>();
   }

   public static bool updateItemSoulBinding (int itemId, bool isBound) {
      return false;
   }

   public static bool isItemSoulBound (int itemId) {
      return false;
   }

   #endregion

   #region Reward Codes

   public static bool useRewardCode (int codeId) {
      return false;
   }

   public static IEnumerable<RewardCode> getRewardCodes (string steamId, string consumerId, string producerId) {
      return Array.Empty<RewardCode>();
   }

   public static IEnumerable<RewardCode> getRewardCodesByProducer (string steamId, string producerId) {
      return Array.Empty<RewardCode>();
   }

   public static IEnumerable<RewardCode> getUnusedRewardCodes (string steamId, string consumerId) {
      return Array.Empty<RewardCode>();
   }

   #endregion

   #region Wishlist

   public static bool notePlayerWishlist (string steamId, string appId) {
      return false;
   }

   public static bool hasPlayerWishlisted (string steamId) {
      return false;
   }

   #endregion

   #region Server Network

   public static int createServerNetworkLargeMessage (byte[] message) {
      return 0;
   }

   public static byte[] getServerNetworkLargeMessage (int messageId) {
      return null;
   }

   public static void deleteServerNetworkLargeMessage (int messageId) {
   }

   #endregion

   #region Server stats

   public static void serverStatStarted (string machine, int port) {
   }

   public static void serverStatStopped (string machine, int port) {
   }

   public static void serverStatUpdateCcu (string machine, int port, int ccu) {
   }

   public static void serverStatUpdateFps (string machine, int port, float fps) {
   }

   #endregion

   public static byte[] fetchWorldMapSector (int sectorIndex) {
      return null;
   }

   public static void uploadWorldMapSpots (IEnumerable<WorldMapSpot> spots) {
   }

   public static IEnumerable<WorldMapSpot> fetchWorldMapSpots () {
      return Array.Empty<WorldMapSpot>();
   }

   public static bool clearWorldMapSpots () {
      return false;
   }

   /*

   public static void refillSupplies (int userId) {

   }

   public static void deleteAccount (int accountId) {

   }

   public static void setSupplies (int shipId, int suppliesAmount) {

   }

   public static List<PortInfo> getPorts () {
      return null;
   }

   public static PortCargoSummary getPortCargoSummary (Barter.Type barterType, int specificPortId = 0) {
      return null;
   }

   public static ShipCargoSummary getShipCargoSummaryForUser (int userId) {
      return null;
   }

   public static int getShipCount (int userId) {
      return 0;
   }

   public static int getPortCargoAmount (int portId, Cargo.Type cargoType, Barter.Type barterType) {
      return 0;
   }

   public static void removeCargoFromPort (int portId, Cargo.Type cargoType, Barter.Type barterType, int amount) {

   }

   public static void addGoldAndXP (int userId, int gold, int XP, int tradePermitChange = 0) {

   }

   public static void addCargoToShip (int shipId, Barter.Type barterType, Cargo.Type cargoType, int amount) {

   }

   public static void deleteEmptyCargoRow (int shipId, Cargo.Type cargoType) {

   }

   public static TradeRecord insertTradeRecord (int userId, int shipId, int portId, Barter.Type barterType, Cargo.Type cargoType, int amount, int unitPrice, int unitXP) {
      return null;
   }

   public static TradeHistory getTradeHistory (int userId) {
      return null;
   }

   public static void incrementTradePermits () {

   }

   public static List<int> getTestUserIds () {
      return null;
   }

   public static List<BuildingLoc> getBuildingLocs () {
      return null;
   }

   public static List<Area> getAreas () {
      return null;
   }

   public static DateTime getLastUnlock (int userId, int areaId, float localX, float localY) {
      return DateTime.MinValue;
   }

   public static void storeUnlock (int accountId, int userId, int areaId, float localX, float localY, int gold, int itemId) {

   }

   public static void setFlagship (int userId, int shipId) {

   }

   public static List<NPC_Info> getNPCs (int areaId) {
      return null;
   }

   public static void insertNPC (NPC_Info info) {

   }

   public static void deleteNPCs () {

   }

   public static List<Shop_ShipInfo> getShopShips () {
      return null;
   }

   public static int insertShip (PortInfo port, Shop_ShipInfo shipInfo) {
      return 0;
   }

   public static void deleteShopShips (PortInfo port) {

   }

   public static List<Shop_ItemInfo> getShopItems () {
      return null;
   }

   public static void deleteShopItems (PortInfo port) {

   }

   public static int insertItem (PortInfo port, Shop_ItemInfo itemInfo) {
      return 0;
   }

   public static ItemInfo createItemFromStock (int userId, Shop_ItemInfo itemStock) {
      return null;
   }

   public static int unlockDiscovery (int userId, Discovery.Type discoveryType, int primaryKeyID, int areaId) {
      return 0;
   }

   public static List<Discovery> getDiscoveries (int userId, int areaId) {
      return null;
   }

   public static List<SeaMonsterSpawnInfo> getSeaMonsterSpawns () {
      return null;
   }

   public static void deleteAllSeaMonsterSpawns () {

   }

   public static int insertSeaMonsterSpawn (int areaId, SeaMonster.Type monsterType, float localX, float localY) {
      return 0;
   }

   public static int insertArea (string areaName, Area.Type areaType, TileType tileType, int worldX, int worldY, int versionNumber, int serverId) {
      return 0;
   }

   public static void deleteOldTreasureAreas () {

   }

   public static void insertServer (string address, int port) {

   }

   public static int getServerId (string address, int port) {
      return 0;
   }

   public static int insertSite (int innerAreaId, int outerAreaId, string siteName, int siteLevel, Site.Type siteType, float localX, float localY) {
      return 0;
   }

   public static SiteLoc getSiteLoc (int siteId) {
      return new SiteLoc();
   }

   public static List<SiteLoc> getSiteLocs () {
      return null;
   }

   */

   #region Private Variables

   #endregion
}

#if IS_SERVER_BUILD == false
public class DB_Main : DB_MainStub { }
#endif
