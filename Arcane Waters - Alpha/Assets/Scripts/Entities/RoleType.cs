﻿// Determines if the unit relies on other entities
public enum RoleType
{
   None = 0,
   Standalone = 1,
   Minion = 2,
   Master = 3,
   AutonomousMinion = 4
}