﻿using System.Collections.Generic;

public class MetricCollection
{
   #region Public Variables

   // The set of metrics included in this collection
   public List<Metric> metrics = new List<Metric>();

   #endregion

   #region Private Variables

   #endregion
}
