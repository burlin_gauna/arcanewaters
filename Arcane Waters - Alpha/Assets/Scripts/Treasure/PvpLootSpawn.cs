﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Mirror;
using MapCreationTool.Serialization;
using System;
using System.Linq;

public class PvpLootSpawn : NetworkBehaviour, IMapEditorDataReceiver {
   #region Public Variables

   // The loot group id reference
   [SyncVar]
   public int lootGroupId;

   // The powerup duration
   public int powerupDuration;

   // If this loot spawner is active
   [SyncVar]
   public bool isActive;

   // The instance id
   [SyncVar]
   public int instanceId;

   // The area key
   [SyncVar]
   public string areaKey;

   // If the powerup is showing
   [SyncVar]
   public bool isShowingPowerup;

   // The powerup visual indicators
   public GameObject powerupIndicator;
   public SpriteRenderer powerupSprite;

   // Reference to powerup type that is non-stackable
   public List<Powerup.Type> nonStackableType = new List<Powerup.Type>();

   // The powerup type this loot spawner will provide
   [SyncVar]
   public Powerup.Type powerupType;

   // List of sprite renderers
   public List<SpriteRenderer> spriteRendererList = new List<SpriteRenderer>();

   // The delay between spawning loot rewards (in seconds)
   public float spawnFrequency = 60;

   // The rarity level
   [SyncVar]
   public Rarity.Type rarity;

   // The rarity frame sprite renderer
   public SpriteRenderer rarityFrame;

   // Reference to the platform holding the spawned powerup
   public GameObject spawnPlatform;

   #endregion

   private void Awake () {
      spriteRendererList = GetComponentsInChildren<SpriteRenderer>(true).ToList();
   }

   private void Start () {
      spriteRendererList = GetComponentsInChildren<SpriteRenderer>(true).ToList();
      StartCoroutine(CO_SetAreaParent());

      // Enable powerups on client side if joining the match right after the active trigger was called
      if (!NetworkServer.active) {
         updatePowerup(isShowingPowerup, (int) powerupType, rarity);
      }
   }

   protected IEnumerator CO_SetAreaParent () {
      // Wait until we have finished instantiating the area
      while (AreaManager.self.getArea(this.areaKey) == null) {
         yield return 0;
      }

      Area area = AreaManager.self.getArea(this.areaKey);
      bool worldPositionStays = area.cameraBounds.bounds.Contains((Vector2) transform.position);
      setAreaParent(area, worldPositionStays);
   }

   public void initializeSpawner (float delay) {
      if (isServer) {
         generatePowerup(delay);
      }
   }

   public void setAreaParent (Area area, bool worldPositionStays) {
      transform.SetParent(area.transform, worldPositionStays);
   }

   private void OnTriggerEnter2D (Collider2D collision) {
      if (NetworkServer.active && isActive) {
         PlayerShipEntity playerEntity = collision.GetComponent<PlayerShipEntity>();
         if (playerEntity != null && playerEntity.instanceId == instanceId) {
            isShowingPowerup = false;
            Powerup powerup = new Powerup() {
                  powerupDuration = powerupDuration,
                  powerupRarity = rarity,
                  powerupType = powerupType,
                  expiry = powerupDuration > 0 ? Powerup.Expiry.Timed : Powerup.Expiry.None,   
            };
            
            if (nonStackableType.Contains(powerup.powerupType)) {
               // Local flag if powerup is replaceable true by default
               bool powerupReplaceable = true;
               
               // Check if user contains powerup with powerup type
               if (PowerupManager.self.doesUserHasPowerupType(playerEntity.userId, powerup.powerupType)) {
                  // Get user's powerup and check if incoming powerup has longer duration, Receive powerup is longer and ignore if not
                  List<Powerup> powerupOfType = PowerupManager.self.getPowerupOfUserWithType(playerEntity.userId, powerup.powerupType);

                  // Check if any of the existing powerups has a higher rarity than the incoming powerup
                  bool hasHigherRarity = powerupOfType.Any(item => item.powerupRarity > powerup.powerupRarity);
                  
                  // Check if any of the existing powerups has the same rarity but higher duration
                  bool sameRarityHigherDuration = powerupOfType.Where(item => item.powerupRarity == powerup.powerupRarity)
                                                               .Any(item => item.powerupDuration > powerup.powerupDuration);
                  
                  // If any of the condition is true, powerup is not replaceable
                  powerupReplaceable = !(hasHigherRarity || sameRarityHigherDuration);                  
               }

               if (powerupReplaceable) {
                  // Replace existing entry with type and create new entry if no entry with type
                  playerEntity.rpc.Target_ReceiveUniquePowerup(powerupType, rarity, collision.transform.position);
                  PowerupManager.self.addReplacePowerupServer(playerEntity.userId, powerup);
               } else {
                  playerEntity.rpc.Target_ReceiveIgnoredPowerup(powerupType, rarity, collision.transform.position);
               }

            } else {
               // Create new entry for this powerup
               playerEntity.rpc.Target_ReceivePowerup(powerupType, rarity, collision.transform.position);
               PowerupManager.self.addPowerupServer(playerEntity.userId, powerup);
            }
            updatePowerup(false, 0, rarity);
            Rpc_ToggleDisplay(false, 0, rarity);
            initializeSpawner(spawnFrequency);
         }
      }
   }

   [Server]
   public void generatePowerup (float delay) {
      Invoke(nameof(delayPowerupTrigger), delay);
   }

   private void delayPowerupTrigger () {
      if (lootGroupId > 0) {
         List<TreasureDropsData> powerupDataList = TreasureDropsDataManager.self.getTreasureDropsById(lootGroupId).FindAll(_ => _.powerUp != Powerup.Type.None);
         if (powerupDataList.Count > 0) {
            TreasureDropsData treasureDropsData = powerupDataList.ChooseRandom();
            powerupType = treasureDropsData.powerUp;
            rarity = treasureDropsData.rarity;
         } else {
            powerupType = Powerup.Type.SpeedUp;
         }
      }
      isShowingPowerup = true;
      updatePowerup(true, (int) powerupType, rarity);
      Rpc_ToggleDisplay(true, (int) powerupType, rarity);
   }

   [ClientRpc]
   public void Rpc_ToggleDisplay (bool isActive, int powerupVal, Rarity.Type rarityType) {
      updatePowerup(isActive, powerupVal, rarityType);
   }

   private void updatePowerup (bool isEnabled, int powerupVal, Rarity.Type rarityType) {
      isActive = isEnabled;
      powerupIndicator.SetActive(isEnabled);

      // Setup rarity frames
      Sprite[] borderSprites = Resources.LoadAll<Sprite>(Powerup.BORDER_SPRITES_LOCATION);

      int offsetIndex = (int) rarityType - 1;
      offsetIndex = Mathf.Clamp(offsetIndex, 0, Enum.GetValues(typeof(Rarity.Type)).Length);
      if (borderSprites.Length > offsetIndex) {
         rarityFrame.sprite = borderSprites[offsetIndex];
      }

      spawnPlatform.SetActive(isEnabled);
      foreach (SpriteRenderer spriteRender in spriteRendererList) {
         spriteRender.enabled = isEnabled;
      }

      if (isEnabled) {
         powerupSprite.sprite = PowerupManager.self.getPowerupData((Powerup.Type) powerupVal).spriteIcon;
      }
   }

   public void receiveData (DataField[] dataFields) {
      foreach (DataField field in dataFields) {
         if (field.k.CompareTo(DataField.LOOT_GROUP_ID) == 0) {
            try {
               lootGroupId = int.Parse(field.v);
            } catch {

            }
         }

         if (field.k.CompareTo(DataField.POWERUP_DURATION) == 0) {
            try {
               powerupDuration = int.Parse(field.v);
            } catch {

            }
         }

         if (field.k.CompareTo(DataField.SPAWN_FREQUENCY) == 0) {
            try {
               spawnFrequency = float.Parse(field.v);
            } catch {

            }
         }
      }
   }

   #region Private Variables

   #endregion
}