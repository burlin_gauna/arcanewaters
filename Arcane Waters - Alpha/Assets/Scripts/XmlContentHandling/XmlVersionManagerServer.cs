﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Mirror;
using UnityEngine.Networking;
using static EditorSQLManager;
using System.IO;
using System;
using System.Text;
using System.Xml.Serialization;
using System.Xml;

public class XmlVersionManagerServer : GenericGameManager {
   #region Public Variables

   // Self
   public static XmlVersionManagerServer self;

   // The slot of the xml data
   public const int XML_SLOT = 3;

   // The current version of the server
   public int newServerVersion;

   // Text directory of the xml text files
   public static string xmlTextDirectory = "/XmlTextFiles";
   public static string serverZipDirectory = "/XmlZipFiles";
   public static string TEXT_FILE_NAME = "textFileName=";
   public static string SERVER_ZIP_FILE = "ServerXmlZip.zip";

   // TABLES (Can be changed)
   public const string ABILITY_TABLE = "ability_xml_v2";
   public const string CROPS_TABLE = "crops_xml_v1";

   public const string RING_TABLE = "equipment_ring_xml_v1";
   public const string NECKLACE_TABLE = "equipment_necklace_xml_v1";
   public const string TRINKET_TABLE = "equipment_trinket_xml_v1";
   public const string ARMOR_TABLE = "equipment_armor_xml_v3";
   public const string WEAPON_TABLE = "equipment_weapon_xml_v3";
   public const string HAT_TABLE = "equipment_hat_xml_v1";
   public const string LAND_MONSTER_TABLE = "land_monster_xml_v3";
   public const string NPC_TABLE = "npc_xml_v2";

   public const string SEA_MONSTER_TABLE = "sea_monster_xml_v2";
   public const string CRAFTING_TABLE = "crafting_xml_v2";

   public const string SHIP_TABLE = "ship_xml_v2";
   public const string SHOP_TABLE = "shop_xml_v2";
   public const string SHIP_ABILITY_TABLE = "ship_ability_xml_v2";
   public const string BACKGROUND_DATA_TABLE = "background_xml_v2";
   public const string TREASURE_DROPS_TABLE = "treasure_drops_xml_v2";

   public const string PERKS_DATA_TABLE = "perks_config_xml";
   public const string QUEST_DATA_TABLE = "quest_data_xml_v1";
   public const string PALETTE_DATA_TABLE = "palette_recolors";
   public const string ITEM_DEFINITIONS_TABLE = "item_definitions";
   public const string PROJECTILES_TABLE = "projectiles_xml_v3";
   public const string TUTORIAL_TABLE = "tutorial_xml_v1";
   public const string MAP_TABLE = "maps_v2";
   public const string SFX_TABLE = "soundeffects_v2";
   public const string HAIRCUTS_TABLE = "haircuts_v2";
   public const string GEMS_TABLE = "gems_bundles_v1";
   public const string SHIP_SKINS_TABLE = "ship_skins_v2";
   public const string CONSUMABLES_TABLE = "consumables_v1";
   public const string DYES_TABLE = "dyes_v1";
   public const string LAND_POWERUP_TABLE = "land_powerup_xml_v1";
   public const string QUEST_ITEMS_TABLE = "quest_items_xml";

   // TEXT FILE NAMES (Do not Modify)
   public const string CROPS_FILE = "crops";
   public const string ABILITIES_FILE = "abilities";
   public const string CRAFTING_FILE = "crafting";

   public const string RING_FILE = "equipment_ring";
   public const string NECKLACE_FILE = "equipment_necklace";
   public const string TRINKET_FILE = "equipment_trinket";
   public const string ARMOR_FILE = "equipment_armor";
   public const string WEAPON_FILE = "equipment_weapon";
   public const string HAT_FILE = "equipment_hat";
   public const string LAND_MONSTER_FILE = "land_monsters";
   public const string NPC_FILE = "npc";

   public const string SEA_MONSTER_FILE = "sea_monsters";

   public const string SHIP_FILE = "ships";
   public const string SHOP_FILE = "shops";
   public const string SHIP_ABILITY_FILE = "ships_abilities";
   public const string BACKGROUND_DATA_FILE = "battle_bg_data";

   public const string PERKS_FILE = "perks";
   public const string PALETTE_FILE = "palettes";
   public const string TREASURE_DROPS_FILE = "treasure_drops";
   public const string QUEST_DATA_FILE = "quest_data";
   public const string ITEM_DEFINITIONS_FILE = "item_definitions";
   public const string TOOL_TIP_FILE = "tool_tip";
   public const string PROJECTILES_FILE = "projectiles_xml";
   public const string TUTORIAL_FILE = "tutorial_xml";
   public const string MAP_FILE = "map_xml";
   public const string SFX_FILE = "sfx_xml";
   public const string HAIRCUTS_FILE = "haircuts";
   public const string GEMS_FILE = "gems";
   public const string SHIP_SKINS_FILE = "ship_skins";
   public const string CONSUMABLES_FILE = "consumables";
   public const string DYES_FILE = "dyes";
   public const string LAND_POWERUPS_FILE = "land_powerups";
   public const string QUEST_ITEMS_FILE = "quest_items";

   // Progress indicators
   public int targetProgress;
   public int currentProgress;

   // Forces the editor to zip the updated xml content
   public bool forceZipNewData;

   // Logs the progress of the file setup
   public bool includeProgressInEditorLog;

   // Stops the server from initializing
   public bool forceDisable;

   #endregion

   protected override void Awake () {
      xmlTextDirectory = Application.persistentDataPath + "/XmlTextFiles";
      serverZipDirectory = Application.persistentDataPath + "/XmlZipFiles";

      base.Awake();
      #if IS_SERVER_BUILD && CLOUD_BUILD
      forceDisable = false;
      D.debug("This is a Server build and a Cloud build, proceed to process xml zip setup");
      #endif
      
      // Make sure this directory exists
      if (!Directory.Exists(xmlTextDirectory)) {
         Directory.CreateDirectory(xmlTextDirectory);
      }
      if (!Directory.Exists(serverZipDirectory)) {
         Directory.CreateDirectory(serverZipDirectory);
      }
      confirmTextFiles();
      self = this;
   }

   public void confirmTextFiles () {
      confirmTextFile(CROPS_FILE);
      confirmTextFile(ABILITIES_FILE);
      confirmTextFile(CRAFTING_FILE);

      confirmTextFile(ARMOR_FILE);
      confirmTextFile(WEAPON_FILE);
      confirmTextFile(HAT_FILE);
      confirmTextFile(RING_FILE);
      confirmTextFile(NECKLACE_FILE);
      confirmTextFile(TRINKET_FILE);
      confirmTextFile(LAND_MONSTER_FILE);
      confirmTextFile(NPC_FILE);

      confirmTextFile(SEA_MONSTER_FILE);

      confirmTextFile(SHIP_FILE);
      confirmTextFile(LAND_POWERUPS_FILE);
      confirmTextFile(SHOP_FILE);
      confirmTextFile(SHIP_ABILITY_FILE);
      confirmTextFile(BACKGROUND_DATA_FILE);

      confirmTextFile(PERKS_FILE);
      confirmTextFile(PALETTE_FILE);
      confirmTextFile(TREASURE_DROPS_FILE);
      confirmTextFile(QUEST_DATA_FILE);
      confirmTextFile(ITEM_DEFINITIONS_FILE);
      confirmTextFile(TOOL_TIP_FILE);
      confirmTextFile(PROJECTILES_FILE);

      confirmTextFile(MAP_FILE);
      confirmTextFile(SFX_FILE);

      confirmTextFile(HAIRCUTS_FILE);
      confirmTextFile(DYES_FILE);
      confirmTextFile(GEMS_FILE);
      confirmTextFile(SHIP_SKINS_FILE);
      confirmTextFile(CONSUMABLES_FILE);
      confirmTextFile(QUEST_ITEMS_FILE);
   }

   private void confirmTextFile (string fileName) {
      string fileDirectory = xmlTextDirectory + "/" + fileName + ".txt";
      if (!File.Exists(fileDirectory)) {
         File.Create(fileDirectory).Close();
      }
   }

   public void initializeServerData () {
      if (!forceDisable) {
         getXmlData();
      }
   }

   private void getXmlData () {
      D.debug("Preparing to Process Files...");
      string compiledData = "";
      int databaseVersion = 0;

      UnityThreadHelper.BackgroundDispatcher.Dispatch(() => {
         compiledData += DB_Main.getLastUpdate(EditorToolType.BattlerAbility);
         compiledData += DB_Main.getLastUpdate(EditorToolType.LandMonster);
         compiledData += DB_Main.getLastUpdate(EditorToolType.LandPowerups);
         compiledData += DB_Main.getLastUpdate(EditorToolType.SeaMonster);
         compiledData += DB_Main.getLastUpdate(EditorToolType.NPC);
         compiledData += DB_Main.getLastUpdate(EditorToolType.Crafting);

         compiledData += DB_Main.getLastUpdate(EditorToolType.Ship);
         compiledData += DB_Main.getLastUpdate(EditorToolType.ShipAbility);

         compiledData += DB_Main.getLastUpdate(EditorToolType.Shop);
         compiledData += DB_Main.getLastUpdate(EditorToolType.Achievement);
         compiledData += DB_Main.getLastUpdate(EditorToolType.Crops);
         compiledData += DB_Main.getLastUpdate(EditorToolType.Background);

         compiledData += DB_Main.getLastUpdate(EditorToolType.Crafting);
         compiledData += DB_Main.getLastUpdate(EditorToolType.Equipment_Armor);
         compiledData += DB_Main.getLastUpdate(EditorToolType.Equipment_Weapon);
         compiledData += DB_Main.getLastUpdate(EditorToolType.Equipment_Hat);
         compiledData += DB_Main.getLastUpdate(EditorToolType.Equipment_Ring);
         compiledData += DB_Main.getLastUpdate(EditorToolType.Equipment_Necklace);
         compiledData += DB_Main.getLastUpdate(EditorToolType.Equipment_Trinket);

         compiledData += DB_Main.getLastUpdate(EditorToolType.Perks);
         compiledData += DB_Main.getLastUpdate(EditorToolType.Palette);
         compiledData += DB_Main.getLastUpdate(EditorToolType.Treasure_Drops);
         compiledData += DB_Main.getLastUpdate(EditorToolType.Quest);
         compiledData += DB_Main.getLastUpdate(EditorToolType.ItemDefinitions);
         compiledData += DB_Main.getLastUpdate(EditorToolType.Projectiles);
         compiledData += DB_Main.getLastUpdate(EditorToolType.Tutorial);
         compiledData += DB_Main.getLastUpdate(EditorToolType.Haircuts);
         compiledData += DB_Main.getLastUpdate(EditorToolType.Dyes);
         compiledData += DB_Main.getLastUpdate(EditorToolType.Gems);
         compiledData += DB_Main.getLastUpdate(EditorToolType.ShipSkins);
         compiledData += DB_Main.getLastUpdate(EditorToolType.Consumables);

         databaseVersion = DB_Main.getLatestXmlVersion();

         UnityThreadHelper.UnityDispatcher.Dispatch(() => {
            compiledData = compiledData.Replace("\n", "");

            // Split each entry data
            string splitter = "[next]";
            string[] xmlGroup = compiledData.Split(new string[] { splitter }, StringSplitOptions.None);

            int index = 1;
            bool shouldZipNewFiles = false;
            foreach (string subgroup in xmlGroup) {
               string subSplitter = "[space]";
               string[] newGroup = subgroup.Split(new string[] { subSplitter }, StringSplitOptions.None);
               if (newGroup.Length == 2) {
                  string xmlTableName = newGroup[0];
                  if (PlayerPrefs.GetString(xmlTableName, "") == "") {
                     shouldZipNewFiles = true;

                     try {
                        DateTime dateTime = Convert.ToDateTime(newGroup[1]);
                     } catch {
                        D.editorLog("Fail: " + newGroup[1], Color.red);
                     }

                     PlayerPrefs.SetString(xmlTableName, newGroup[1]);
                  } else {
                     DateTime savedDate = DateTime.UtcNow;
                     try {
                        savedDate = Convert.ToDateTime(PlayerPrefs.GetString(xmlTableName));
                     } catch {
                        D.debug("Failed to convert cached date time for xmlTable: " + xmlTableName);
                     }
                     DateTime serverDate = DateTime.UtcNow;

                     try {
                        serverDate = Convert.ToDateTime(newGroup[1]);
                     } catch {
                        D.editorLog("Failed to convert: " + newGroup[1], Color.red);
                     }

                     debugLog(xmlTableName + "  There are saved files: Old:" + savedDate + " - New: " + serverDate, Color.cyan);

                     if (serverDate > savedDate) {
                        shouldZipNewFiles = true;
                        PlayerPrefs.SetString(xmlTableName, serverDate.ToString());
                        D.debug("Server updated recently updates: " + (index + "/" + (xmlGroup.Length - 1)));
                     } else {
                        debugLog("NO updates: " + (index + "/" + (xmlGroup.Length - 1)), Color.blue);
                     }
                  }
               }
               index++;
            }

            // Set the latest version
            newServerVersion = databaseVersion + 1;

            if (forceZipNewData) {
               shouldZipNewFiles = true;
            }

            string serverMessage = "";
            if (shouldZipNewFiles) {
               serverMessage = "Version is valid: Writing data to server, new version from: " + databaseVersion + " to " + newServerVersion;
               targetProgress = 0;
               currentProgress = 0;

               D.debug("Zipping Process has begun");
               beginZipProcess();
            } else {
               serverMessage = "No new version, set as cached version: " + databaseVersion;
            }

            debugLog(serverMessage, Color.cyan);
         });
      });
   }

   private void beginZipProcess () {
      UnityThreadHelper.BackgroundDispatcher.Dispatch(() => {
         // Get contents from DB_Main
         string landMonsterData = DB_Main.getXmlContent(LAND_MONSTER_TABLE);
         string seaMonsterData = DB_Main.getXmlContent(SEA_MONSTER_TABLE);
         string craftingData = DB_Main.getXmlContent(CRAFTING_TABLE);

         string npcData = DB_Main.getXmlContent(NPC_TABLE);
         string cropsData = DB_Main.getXmlContent(CROPS_TABLE);
         string abilityData = DB_Main.getXmlContent(ABILITY_TABLE);
         string landPowerupData = DB_Main.getXmlContent(LAND_POWERUP_TABLE, EditorToolType.LandPowerups);

         string armorData = DB_Main.getXmlContent(ARMOR_TABLE, EditorToolType.Equipment_Armor);
         string weaponData = DB_Main.getXmlContent(WEAPON_TABLE, EditorToolType.Equipment_Weapon);
         string hatData = DB_Main.getXmlContent(HAT_TABLE, EditorToolType.Equipment_Hat);
         string ringData = DB_Main.getXmlContent(RING_TABLE, EditorToolType.Equipment_Ring);
         string necklaceData = DB_Main.getXmlContent(NECKLACE_TABLE, EditorToolType.Equipment_Necklace);
         string trinkerData = DB_Main.getXmlContent(TRINKET_TABLE, EditorToolType.Equipment_Trinket);

         string shopData = DB_Main.getXmlContent(SHOP_TABLE, EditorToolType.Shop);
         string shipData = DB_Main.getXmlContent(SHIP_TABLE, EditorToolType.Ship);
         string shipAbilityData = DB_Main.getXmlContent(SHIP_ABILITY_TABLE);
         string battleBGData = DB_Main.getXmlContent(BACKGROUND_DATA_TABLE);

         string perkData = DB_Main.getXmlContent(PERKS_DATA_TABLE);
         string tutorialData = DB_Main.getXmlContent(TUTORIAL_TABLE, EditorToolType.Tutorial);

         // Manually assign the xml data of the perks content due to its data struct not uniformed
         string paletteData = overridePaletteDataXML();
         string treasureDropsData = DB_Main.getXmlContent(TREASURE_DROPS_TABLE, EditorToolType.Treasure_Drops);
         string questData = DB_Main.getXmlContent(QUEST_DATA_TABLE, EditorToolType.Quest);
         string itemDefinitionsData = DB_Main.getXmlContent(ITEM_DEFINITIONS_TABLE, EditorToolType.ItemDefinitions);
         string tooltipData = DB_Main.getTooltipXmlContent();
         string projectileData = DB_Main.getXmlContent(PROJECTILES_TABLE, EditorToolType.Projectiles);
         string mapData = DB_Main.getMapContents();
         List<SoundEffect> soundEffectsRawData = DB_Main.getSoundEffects();
         string soundEffectsData = SoundEffectManager.self.getSoundEffectsStringData(soundEffectsRawData);

         string haircutsData = DB_Main.getXmlContent(HAIRCUTS_TABLE, EditorToolType.Haircuts);
         string dyesData = DB_Main.getXmlContent(DYES_TABLE, EditorToolType.Dyes);
         string gemsData = DB_Main.getXmlContent(GEMS_TABLE, EditorToolType.Gems);
         string shipSkinsData = DB_Main.getXmlContent(SHIP_SKINS_TABLE, EditorToolType.ShipSkins);
         string consumablesData = DB_Main.getXmlContent(CONSUMABLES_TABLE, EditorToolType.Consumables);

         string questItemsData = DB_Main.getXmlContent(QUEST_ITEMS_TABLE, EditorToolType.QuestItems);

         // Write data to text files
         writeAndCache(xmlTextDirectory + "/" + LAND_MONSTER_FILE + ".txt", landMonsterData);
         writeAndCache(xmlTextDirectory + "/" + SEA_MONSTER_FILE + ".txt", seaMonsterData);
         writeAndCache(xmlTextDirectory + "/" + CRAFTING_FILE + ".txt", craftingData);

         writeAndCache(xmlTextDirectory + "/" + NPC_FILE + ".txt", npcData);
         writeAndCache(xmlTextDirectory + "/" + CROPS_FILE + ".txt", cropsData);
         writeAndCache(xmlTextDirectory + "/" + ABILITIES_FILE + ".txt", abilityData);

         writeAndCache(xmlTextDirectory + "/" + ARMOR_FILE + ".txt", armorData);
         writeAndCache(xmlTextDirectory + "/" + WEAPON_FILE + ".txt", weaponData);
         writeAndCache(xmlTextDirectory + "/" + HAT_FILE + ".txt", hatData);
         writeAndCache(xmlTextDirectory + "/" + RING_FILE + ".txt", ringData);
         writeAndCache(xmlTextDirectory + "/" + NECKLACE_FILE + ".txt", necklaceData);
         writeAndCache(xmlTextDirectory + "/" + TRINKET_FILE + ".txt", trinkerData);

         writeAndCache(xmlTextDirectory + "/" + SHOP_FILE + ".txt", shopData);
         writeAndCache(xmlTextDirectory + "/" + LAND_POWERUPS_FILE + ".txt", landPowerupData);
         writeAndCache(xmlTextDirectory + "/" + SHIP_FILE + ".txt", shipData);
         writeAndCache(xmlTextDirectory + "/" + SHIP_ABILITY_FILE + ".txt", shipAbilityData);
         writeAndCache(xmlTextDirectory + "/" + BACKGROUND_DATA_FILE + ".txt", battleBGData);

         writeAndCache(xmlTextDirectory + "/" + PERKS_FILE + ".txt", perkData);
         writeAndCache(xmlTextDirectory + "/" + PALETTE_FILE + ".txt", paletteData);
         writeAndCache(xmlTextDirectory + "/" + TREASURE_DROPS_FILE + ".txt", treasureDropsData);
         writeAndCache(xmlTextDirectory + "/" + QUEST_DATA_FILE + ".txt", questData);
         writeAndCache(xmlTextDirectory + "/" + ITEM_DEFINITIONS_FILE + ".txt", itemDefinitionsData);
         writeAndCache(xmlTextDirectory + "/" + TOOL_TIP_FILE + ".txt", tooltipData);
         writeAndCache(xmlTextDirectory + "/" + PROJECTILES_FILE + ".txt", projectileData);
         writeAndCache(xmlTextDirectory + "/" + TUTORIAL_FILE + ".txt", tutorialData);
         writeAndCache(xmlTextDirectory + "/" + MAP_FILE + ".txt", mapData);
         writeAndCache(xmlTextDirectory + "/" + SFX_FILE + ".txt", soundEffectsData);

         writeAndCache(xmlTextDirectory + "/" + HAIRCUTS_FILE + ".txt", haircutsData);
         writeAndCache(xmlTextDirectory + "/" + DYES_FILE + ".txt", dyesData);
         writeAndCache(xmlTextDirectory + "/" + GEMS_FILE + ".txt", gemsData);
         writeAndCache(xmlTextDirectory + "/" + SHIP_SKINS_FILE + ".txt", shipSkinsData);
         writeAndCache(xmlTextDirectory + "/" + CONSUMABLES_FILE + ".txt", consumablesData);
         writeAndCache(xmlTextDirectory + "/" + QUEST_ITEMS_FILE + ".txt", questItemsData);

         UnityThreadHelper.UnityDispatcher.Dispatch(() => {
            string zipDirectory = serverZipDirectory + "/" + SERVER_ZIP_FILE;
            GZipUtility.compressDirectory(xmlTextDirectory + "/", zipDirectory, (fileName) => { debugLog("Compressing ..." + fileName, Color.gray); });

            finalizeZipData();
         });
      });
   }

   private string overridePaletteDataXML () {
      string newContent = "";
      List<RawPaletteToolData> paletteDatabaseContent = DB_Main.getPaletteXmlContent(PALETTE_DATA_TABLE);

      foreach (RawPaletteToolData paletteData in paletteDatabaseContent) {
         PaletteToolData xmlData = Util.xmlLoad<PaletteToolData>(paletteData.xmlData);

         // Manually inject these values to the newly fetched xml translated data
         xmlData.subcategory = paletteData.subcategory;
         xmlData.tagId = paletteData.tagId;

         // Translate to xml again, this time the class has the updated subcategory and tag
         XmlSerializer ser = new XmlSerializer(xmlData.GetType());
         var sb = new StringBuilder();
         using (var writer = XmlWriter.Create(sb)) {
            ser.Serialize(writer, xmlData);
         }
         string newXmlData = sb.ToString();

         newContent += paletteData.xmlId + "[space]" + newXmlData + "[next]\n";
      }
      return newContent;
   }

   private void writeAndCache (string fileName, string fileContent) {
      File.WriteAllText(fileName, fileContent);
      _filenamesAndData.Add(fileName, fileContent);
   }

   private void finalizeZipData () {
      debugLog("Init Zip data sending", Color.green);
      string zipDirectory = serverZipDirectory + "/" + SERVER_ZIP_FILE;
      UnityThreadHelper.BackgroundDispatcher.Dispatch(() => {
         byte[] zipData = File.ReadAllBytes(zipDirectory);
         DB_Main.writeZipData(zipData, XML_SLOT);

         UnityThreadHelper.UnityDispatcher.Dispatch(() => {
            D.debug("Zip Upload Complete: Windows:" + zipData.Length);
         });
      });
   }

   private void debugLog (string message, Color color) {
      if (includeProgressInEditorLog) {
         try {
            D.debug(message);
         } catch {
            D.editorLog(message, Color.cyan);
         }
      }
   }

   #region Private Variables

   // The file data gathered for zipping
   private Dictionary<string, string> _filenamesAndData = new Dictionary<string, string>();

   #endregion
}