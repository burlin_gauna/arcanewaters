﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Mirror;
using System;
using System.Linq;

public class InstanceManager : MonoBehaviour
{
   #region Public Variables

   // The prefab we use for creating Instances
   public Instance instancePrefab;

   // Self
   public static InstanceManager self;

   #endregion

   private void Awake () {
      self = this;
   }

   public Instance addPlayerToInstance (NetEntity player, string areaKey, int groupInstanceId, int instanceToVisit = -1) {
      Instance instance = null;

      // Fetch the instance declared by server if there is any
      if (instanceToVisit > 0 && getInstance(instanceToVisit) != null) {
         instance = getInstance(instanceToVisit);
         if (instance != null) {
            D.adminLog("User {" + player.entityName + ":" + player.userId + "} " +
               "successfully fetched visit instance {" + instanceToVisit + ":" + areaKey + "} {" + instance.privateAreaUserId + "}", D.ADMIN_LOG_TYPE.Visit);
         } else {
            D.adminLog("User {" + player.entityName + ":" + player.userId + "} " +
               "Failed fetched visit instance {" + instanceToVisit + ":" + areaKey + "}", D.ADMIN_LOG_TYPE.Visit);
         }
      }

      // If the player is warping to a world map instance, search for it
      if (GroupInstanceManager.isWorldMapArea(areaKey) && WorldMapManager.isWorldMapArea(areaKey)) {
         Instance existingInstance = getWorldMapOpenInstance(areaKey);
         if (existingInstance != null) {
            instance = existingInstance;
         }
      }

      // If the player is warping to a group instance, search for it
      if (groupInstanceId != -1) {
         if (GroupInstanceManager.isAnyLeagueArea(areaKey) || GroupInstanceManager.isPvpArenaArea(areaKey)) {
            if (!tryGetGroupInstance(groupInstanceId, out instance)) {
               D.error("Could not find the group instance with id " + groupInstanceId + " in area " + areaKey);
            }

            // TODO: Generate a new group instance for user here
            int pvpInstanceId = GroupInstanceManager.self.getPvpInstanceId(areaKey);
            if (pvpInstanceId > 0) {
               Instance inst = getInstance(pvpInstanceId);
               if (inst != null) {
                  instance = inst;
               } else {
                  D.debug("Failed to get pvp instance: " + pvpInstanceId);
               }
            }
         } else if (GroupInstanceManager.isTreasureSiteArea(areaKey)) {
            // Search for the parent sea instance
            if (tryGetGroupInstance(groupInstanceId, out Instance seaGroupInstance)) {
               // Search for the treasure site entrance the player is registered in
               foreach (TreasureSite treasureSite in seaGroupInstance.treasureSites) {
                  if (treasureSite.playerListInSite.Contains(player.userId)) {
                     // Check if the treasure site instance has already been created
                     if (treasureSite.destinationInstanceId > 0) {
                        D.adminLog("Created New Instance for Treasure Site", D.ADMIN_LOG_TYPE.InstanceProcess);
                        instance = getInstance(treasureSite.destinationInstanceId);

                        if (instance != null) {
                           D.adminLog("TreasureSite instance being fetched here, " +
                              "TreasureSite: {" + treasureSite.areaKey + "} {" + treasureSite.instanceId + "} {" + treasureSite.destinationInstanceId + "} " +
                              "PrevArea: {" + areaKey + " } {" + player.areaKey + "}  {" + player.instanceId + "}" +
                              "Instance: {" + instance.id + "}{" + instance.areaKey + "}{" + instance.groupInstanceId + "}", D.ADMIN_LOG_TYPE.POI_WARP);

                           if (areaKey != instance.areaKey) {
                              D.adminLog("Created New Instance for POI, area keys did not match for " +
                                 "InstanceId:{" + instance.id + "} InstanceArea:{" + instance.areaKey + "} TargetArea:{" + areaKey + "}", D.ADMIN_LOG_TYPE.POI_WARP);

                              // If the treasure site instance doesn't exist yet, create it
                              instance = createNewInstance(areaKey, false, groupInstanceId, seaGroupInstance.voyageExitAreaKey, seaGroupInstance.voyageExitSpawnKey, seaGroupInstance.voyageExitFacingDirection);
                              treasureSite.destinationInstanceId = instance.id;
                           }
                        }
                     } else {
                        D.adminLog("Created New Instance for Non-Treasure Site", D.ADMIN_LOG_TYPE.InstanceProcess);
                        // If the treasure site instance doesn't exist yet, create it
                        instance = createNewInstance(areaKey, false, groupInstanceId, seaGroupInstance.voyageExitAreaKey, seaGroupInstance.voyageExitSpawnKey, seaGroupInstance.voyageExitFacingDirection);
                        treasureSite.destinationInstanceId = instance.id;
                     }
                     break;
                  }
               }

               // If the user is not registered in a treasure site entrance, try to find an existing instance with the same id
               if (instance == null) {
                  tryGetTreasureSiteInstance(groupInstanceId, areaKey, out instance);
               }

               // If all else fails, create a new instance (useful for /admin warp commands)
               if (instance == null) {
                  instance = createNewInstance(areaKey, false, groupInstanceId, "", "", Direction.South);
                  D.adminLog("Created New Instance for treasure site", D.ADMIN_LOG_TYPE.InstanceProcess);
               }
            }
         } else if (GroupInstanceManager.isPOIArea(areaKey)) {
            if (!POISiteManager.self.tryGetInstanceForGroup(player.groupId, areaKey, out instance)) {
               if (player.isAdmin() && tryGetGroupInstance(groupInstanceId, out instance)) {
                  // If the user is admin and the target group instance instance exists, let him spawn even if the group is not linked to a POI site (useful for admin warp and goto commands)
               } else {
                  D.warning($"Could not determine the POI instance for player {player.userId}, area {areaKey}");
               }
            }
         }
      } else {
         if (GroupInstanceManager.isPvpArenaArea(areaKey)) {
            int pvpInstanceId = GroupInstanceManager.self.getPvpInstanceId(areaKey);
            if (pvpInstanceId > 0) {
               Instance inst = getInstance(pvpInstanceId);
               if (inst != null) {
                  instance = inst;
               }
            }
         }
      }

      // Search for an existing private instance assigned to this user or create a new one
      if (AreaManager.self.isPrivateArea(areaKey)) {
         instance = getPlayerPrivateOpenInstance(areaKey, player.userId);
         if (instance == null) {
            instance = createNewInstance(areaKey, player.isSinglePlayer);
            // Register the user id as the privateAreaUserId
            instance.privateAreaUserId = player.userId;
         }
      }

      if (instance == null) {
         // Look for an open instance
         instance = getOpenInstance(areaKey, player.isSinglePlayer);
         if (instance != null) {
            D.adminLog("Get Open Instance", D.ADMIN_LOG_TYPE.InstanceProcess);
         }
      }

      // Fetch existing visitable custom map here if the target area is a custom map, for visit feature
      if (instance == null && CustomMapManager.isPrivateCustomArea(areaKey)) {
         Instance openInst = getVisitablePrivateOpenInstance(areaKey);
         if (openInst) {
            instance = openInst;
         }
      }

      // If there isn't one, we'll have to make it
      if (instance == null) {
         instance = createNewInstance(areaKey, player.isSinglePlayer);
         if (CustomMapManager.isPrivateCustomArea(areaKey)) {
            instance.privateAreaUserId = player.userId;
         }
      }

      // Set the player's instance ID
      player.instanceId = instance.id;

      // Add the player to the list
      instance.entities.Add(player);

      // If player is entering a farm, we need to load plantable trees for him
      if (AreaManager.self.tryGetCustomMapManager(areaKey, out CustomMapManager cmm)) {
         if (cmm is CustomFarmManager || cmm is CustomGuildMapManager) {
            PlantableTreeManager.self.playerEnteredFarm(player, areaKey);
         }
      }

      // Allow discovery manager to react
      DiscoveryManager.self.userEntersInstance(player);

      // If they're entering their farm, send them their crops
      if (AreaManager.self.tryGetCustomMapManager(areaKey, out CustomMapManager customMapManager)) {
         CustomFarmManager farmManager = customMapManager as CustomFarmManager;
         CustomGuildMapManager guildMapManager = customMapManager as CustomGuildMapManager;
         if (farmManager != null) {
            int userId = CustomMapManager.isUserSpecificAreaKey(areaKey) ? CustomMapManager.getUserId(areaKey) : player.userId;
            player.cropManager.loadCrops(userId);
         } else if (guildMapManager != null) {
            int guildId = CustomMapManager.getGuildId(areaKey);
            player.cropManager.loadGuildCrops(guildId);
            D.debug("Entered custom guild map. Loading crops for guild: " + guildId);
         }
      } else {
         // TODO: Update this block of code after setting up farm maps
         if (areaKey.ToLower().Contains("farm")) {
            player.cropManager.loadCrops(player.userId);
         }
      }

      return instance;
   }

   public void addVaryingStateObjectToInstance (VaryingStateObject ob, Instance instance) {
      instance.entities.Add(ob);
      ob.instanceId = instance.id;
   }

   public void addCustomizationManagerToInstance (MapCustomizationManager mapCustomizationManager, Instance instance) {
      instance.entities.Add(mapCustomizationManager);
      instance.mapCustomizationManager = mapCustomizationManager;
      mapCustomizationManager.instanceId = instance.id;
   }

   public void addDroppedItemToInstance (DroppedItem item, Instance instance) {
      instance.entities.Add(item);
      item.instanceId = instance.id;
   }

   public void addDiscoveryToInstance (Discovery discovery, Instance instance) {
      instance.entities.Add(discovery);
      discovery.instanceId = instance.id;
   }

   public void addEnemyToInstance (Enemy enemy, Instance instance) {
      instance.entities.Add(enemy);
      enemy.instanceId = instance.id;
      instance.enemyCount++;
   }

   public void addNPCToInstance (NPC npc, Instance instance) {
      instance.entities.Add(npc);
      npc.instanceId = instance.id;
      instance.npcCount++;
   }

   public void addSeaMonsterToInstance (SeaEntity seaMonster, Instance instance) {
      instance.entities.Add(seaMonster);
      seaMonster.instanceId = instance.id;
      instance.seaMonsterCount++;
   }

   public void addTreasureSiteToInstance (TreasureSite treasureSite, Instance instance) {
      instance.entities.Add(treasureSite);
      instance.treasureSites.Add(treasureSite);
      treasureSite.instanceId = instance.id;
      instance.treasureSiteCount++;
   }

   public void addInteractableToInstance (InteractableObjEntity objEntity, Instance instance) {
      instance.entities.Add(objEntity);
      instance.interactableObject.Add(objEntity);
      objEntity.instanceId = instance.id;
   }

   public void addOutpostToInstance (Outpost outpost, Instance instance) {
      addSeaStructureToInstance(outpost, instance);
   }

   public void addSeaStructureToInstance (SeaStructure seaStructure, Instance instance) {
      instance.entities.Add(seaStructure);
      instance.seaStructures.Add(seaStructure);
      seaStructure.instanceId = instance.id;
      instance.seaStructureCount++;
   }

   public Outpost getOutpost (Instance instance, int guildId, string areaKey) {
      foreach (SeaStructure entityRef in instance.seaStructures) {
         if (entityRef is Outpost) {
            Outpost newOutpost = (Outpost) entityRef;
            if (newOutpost.guildId == guildId && areaKey == newOutpost.areaKey) {
               return newOutpost;
            }
         }
      }
      return null;
   }

   public void addSeaEntityToInstance (SeaEntity seaEntity, Instance instance) {
      instance.entities.Add(seaEntity);
      seaEntity.instanceId = instance.id;
   }

   public void addSeaMonsterSpawnerToInstance (PvpMonsterSpawner monsterSpawner, Instance instance) {
      instance.entities.Add(monsterSpawner);
      instance.pvpMonsterSpawners.Add(monsterSpawner);
      monsterSpawner.instanceId = instance.id;
   }

   public void addWindowToInstance (WindowInteractable windowInteractable, Instance instance) {
      instance.entities.Add(windowInteractable);
      windowInteractable.instanceId = instance.id;
   }

   public void addPvpWaypointToInstance (PvpWaypoint waypoint, Instance instance) {
      instance.pvpWaypoints.Add(waypoint);
   }

   public void addLootSpawnerToInstance (PvpLootSpawn lootSpawn, Instance instance) {
      instance.entities.Add(lootSpawn);
      instance.lootSpawners.Add(lootSpawn);
      lootSpawn.instanceId = instance.id;
   }

   public Instance getInstance (int instanceId) {
      if (_instances.ContainsKey(instanceId)) {
         return _instances[instanceId];
      }

      return null;
   }

   public bool tryGetInstance (int instanceId, out Instance instance) {
      if (_instances.TryGetValue(instanceId, out instance)) {
         return true;
      }

      return false;
   }

   public List<Instance> getAllInstances () {
      return _instances.Values.ToList();
   }

   public Instance createNewInstance (string areaKey, bool isSinglePlayer, int groupInstanceIdOverride = -1) {
      return createNewInstance(areaKey, isSinglePlayer, groupInstanceIdOverride, "", "", Direction.South);
   }

   public Instance createNewInstance (string areaKey, bool isSinglePlayer, int groupInstanceId, string voyageExitAreaKey, string voyageExitSpawnKey, Direction voyageExitFacingDirection) {
      int difficulty = getDifficultyForInstance(groupInstanceId);
      return createNewInstance(areaKey, isSinglePlayer, groupInstanceId, difficulty, voyageExitAreaKey, voyageExitSpawnKey, voyageExitFacingDirection);
   }

   public Instance createNewInstance (string areaKey, bool isSinglePlayer, int groupInstanceId, int difficulty, string voyageExitAreaKey, string voyageExitSpawnKey, Direction voyageExitFacingDirection) {
      Biome.Type biome = getBiomeForInstance(areaKey, groupInstanceId);
      return createNewInstance(areaKey, isSinglePlayer, false, groupInstanceId, false, false, 0, -1, voyageExitAreaKey, voyageExitSpawnKey, voyageExitFacingDirection, difficulty, biome);
   }

   public Instance createNewInstance (string areaKey, bool isSinglePlayer, bool isGroupInstance, int groupInstanceId, bool isPvP, bool isLeague, int leagueIndex, int leagueRandomSeed, string voyageExitAreaKey, string voyageExitSpawnKey, Direction voyageExitFacingDirection, int difficulty, Biome.Type biome) {
      Instance instance = Instantiate(instancePrefab, this.transform);
      instance.id = _id++;
      instance.areaKey = areaKey;
      instance.numberInArea = getInstanceCount(areaKey) + 1;
      instance.serverAddress = MyNetworkManager.self.networkAddress;
      instance.serverPort = MyNetworkManager.self.Port;
      instance.mapSeed = UnityEngine.Random.Range(0, 1000000);
      instance.creationDate = DateTime.UtcNow.ToBinary();
      instance.isGroupInstance = isGroupInstance;
      instance.isLeague = isLeague;
      instance.leagueIndex = leagueIndex;
      instance.leagueRandomSeed = leagueRandomSeed;
      instance.voyageExitAreaKey = voyageExitAreaKey;
      instance.voyageExitSpawnKey = voyageExitSpawnKey;
      instance.voyageExitFacingDirection = voyageExitFacingDirection;
      instance.groupInstanceId = groupInstanceId;
      instance.biome = biome == Biome.Type.None ? AreaManager.self.getDefaultBiome(areaKey) : biome;
      instance.isPvP = isPvP;
      instance.difficulty = difficulty;
      instance.isSinglePlayer = isSinglePlayer;
      instance.updateMaxPlayerCount(isSinglePlayer);

      if (CustomMapManager.isPrivateCustomArea(areaKey)) {
         int ownerUserId = CustomMapManager.getUserId(areaKey);

         if (instance.areaKey.Contains(CustomFarmManager.GROUP_AREA_KEY)) {
            ServerNetworkingManager.self.addPrivateFarmInstance(ownerUserId);
         } else if (instance.areaKey.Contains(CustomHouseManager.GROUP_AREA_KEY)) {
            ServerNetworkingManager.self.addPrivateHouseInstance(ownerUserId);
         }

         D.adminLog("Generated new instance: {" + areaKey + "} Owner:{" + ownerUserId + "} InstanceId:{" + instance.id + "}", D.ADMIN_LOG_TYPE.Visit);
      }

      // Keep track of it
      _instances.Add(instance.id, instance);

      // Immediately make the new instance info accessible to other servers
      if (ServerNetworkingManager.self != null && ServerNetworkingManager.self.server != null) {
         ServerNetworkingManager.self.server.updateInstance(instance.getBasicInstanceOverview());
         if (groupInstanceId > 0) {
            ServerNetworkingManager.self.server.updateGroupInstance(instance);
         }
      }

      // Spawn the network object on the Clients
      if (NetworkServer.active) {
         NetworkServer.Spawn(instance.gameObject);
      }
      return instance;
   }

   public Instance getOpenInstance (string areaKey, bool isSinglePlayer) {
      if (!isSinglePlayer) {
         foreach (Instance instance in _instances.Values) {
            if (instance.areaKey == areaKey && instance.getPlayerCount() < instance.getMaxPlayers() && !instance.isSinglePlayer) {
               return instance;
            }
         }
      }

      return null;
   }

   public Instance getWorldMapOpenInstance (string areaKey) {
      foreach (Instance instance in _instances.Values) {
         if (WorldMapManager.isWorldMapArea(instance.areaKey)) {
            if (instance.areaKey == areaKey && instance.getPlayerCount() < instance.getMaxPlayers()) {
               return instance;
            }
         }
      }

      return null;
   }

   public Instance getPlayerPrivateOpenInstance (string areaKey, int userId) {
      foreach (Instance instance in _instances.Values) {
         if (instance.areaKey == areaKey && instance.getPlayerCount() < instance.getMaxPlayers() && instance.privateAreaUserId == userId) {
            return instance;
         }
      }

      return null;
   }

   public Instance getVisitablePrivateOpenInstance (string areaKey) {
      foreach (Instance instance in _instances.Values) {
         if (instance.areaKey == areaKey) {
            return instance;
         }
      }

      return null;
   }

   public Instance getVisitablePrivateOpenInstance (int userId) {
      foreach (Instance instance in _instances.Values) {
         if (instance.getPlayerUserIds().Exists(_ => _ == userId)) {
            return instance;
         }
      }

      return null;
   }

   public void removeEntityFromInstance (NetEntity entity) {
      if (entity == null || entity.instanceId == 0) {
         D.adminLog("No need to remove entity from instance: " + entity, D.ADMIN_LOG_TYPE.Disconnection);
         return;
      }

      // Look up the Instance object, and remove the entity from that
      if (_instances.ContainsKey(entity.instanceId)) {
         Instance instance = _instances[entity.instanceId];
         instance.removeEntityFromInstance(entity);

         // Make the other entities in this instance update their observer list
         rebuildInstanceObservers(entity, instance);
      }

      // Mark the entity as no longer having an instance
      entity.instanceId = 0;
   }

   public void rebuildInstanceObservers (NetEntity newEntity, Instance instance) {
      // If this entity doesn't have a client connection, then all it needs to do is make itself visible to clients in the instance
      if (newEntity.connectionToClient == null) {
         newEntity.netIdent.RebuildObservers(false);
      } else {
         // Everything in the instance needs to update its observer list to include the new entity
         foreach (NetworkBehaviour instanceEntity in instance.entities) {
            if (instanceEntity != null) {
               instanceEntity.netIdentity.RebuildObservers(false);
            }
         }

         // We also need the Instance object to be viewable by the new player
         instance.netIdent.RebuildObservers(false);
      }
   }

   public string[] getOpenAreas () {
      HashSet<string> openAreas = new HashSet<string>();

      foreach (Instance instance in _instances.Values) {
         if (instance.getPlayerCount() < instance.getMaxPlayers()) {
            openAreas.Add(instance.areaKey);
         }
      }

      string[] areaArray = new string[openAreas.Count];
      openAreas.CopyTo(areaArray);

      return areaArray;
   }

   public string[] getAreas () {
      HashSet<string> areas = new HashSet<string>();

      foreach (Instance instance in _instances.Values) {
         areas.Add(instance.areaKey);
      }

      string[] areaArray = new string[areas.Count];
      areas.CopyTo(areaArray);

      return areaArray;
   }

   public List<Instance> getGroupInstances () {
      List<Instance> groupInstances = new List<Instance>();

      foreach (Instance instance in _instances.Values) {
         if (instance.isGroupInstance) {
            groupInstances.Add(instance);
         }
      }

      return groupInstances;
   }

   public List<Instance> getTreasureSiteInstancesLinkedToVoyages () {
      List<Instance> treasureSiteInstances = new List<Instance>();

      foreach (Instance instance in _instances.Values) {
         if (GroupInstanceManager.isTreasureSiteArea(instance.areaKey) && instance.groupInstanceId > 0) {
            treasureSiteInstances.Add(instance);
         }
      }

      return treasureSiteInstances;
   }

   public bool tryGetGroupInstance (int groupInstanceId, out Instance instance) {
      instance = null;

      foreach (Instance i in _instances.Values) {
         if (i.isGroupInstance && i.groupInstanceId == groupInstanceId) {
            instance = i;
            return true;
         }
      }

      return false;
   }

   public bool tryGetTreasureSiteInstance (int groupInstanceId, string areaKey, out Instance instance) {
      instance = null;

      foreach (Instance i in _instances.Values) {
         if (GroupInstanceManager.isTreasureSiteArea(i.areaKey) && i.groupInstanceId == groupInstanceId && string.Equals(i.areaKey, areaKey)) {
            instance = i;
            return true;
         }
      }

      return false;
   }

   public static Biome.Type getBiomeForInstance (string areaKey, int groupInstanceId) {
      // Group instances can have a biome different than the default for the area
      if (GroupInstanceManager.isAnyLeagueArea(areaKey) || GroupInstanceManager.isPvpArenaArea(areaKey) || GroupInstanceManager.isTreasureSiteArea(areaKey)) {
         if (GroupInstanceManager.self.tryGetGroupInstance(groupInstanceId, out GroupInstance groupInstance)) {
            return groupInstance.biome;
         }
      }

      return AreaManager.self.getDefaultBiome(areaKey);
   }

   public static int getDifficultyForInstance (int groupInstanceId) {
      if (GroupInstanceManager.self.tryGetGroupInstance(groupInstanceId, out GroupInstance groupInstance)) {
         return groupInstance.difficulty;
      }

      return 1;
   }

   public int getInstanceCount (string areaKey, bool ignoreSinglePlayerInstances = false) {
      int count = 0;

      foreach (Instance instance in _instances.Values) {
         if (instance.areaKey == areaKey && !(instance.isSinglePlayer && ignoreSinglePlayerInstances)) {
            count++;
         }
      }

      return count;
   }

   public bool isPrimaryInstance (Instance instance) {
      foreach (Instance existing in _instances.Values) {
         if (existing.areaKey.CompareTo(instance.areaKey) == 0) {
            if (existing.creationDate < instance.creationDate) {
               return false;
            }
         }
      }

      return true;
   }

   public bool getPrimaryInstance (string areaKey) {
      Instance result = null;
      long oldest = long.MaxValue;

      foreach (Instance instance in _instances.Values) {
         if (instance.areaKey.CompareTo(areaKey) == 0) {
            if (instance.creationDate < oldest) {
               oldest = instance.creationDate;
               result = instance;
            }
         }
      }

      return result;
   }

   public void reset () {
      _instances.Clear();
   }

   public bool hasActiveInstanceForArea (string areaKey) {
      foreach (Instance instance in _instances.Values) {
         if (instance.areaKey == areaKey && instance.entityCount > 0) {
            return true;
         }
      }

      return false;
   }

   public Instance getClientInstance (int playerInstanceId) {
      // TODO: Confirm if registering each instance on startup for each client is ideal than getting component in children, since instance registry only takes place in server side(createNewInstance())
      List<Instance> instances = GetComponentsInChildren<Instance>().ToList();
      return instances.Find(_ => _.id == playerInstanceId);
   }

   public void removeEmptyInstance (Instance instance) {
      string areaKey = instance.areaKey;

      // Make sure there's no one inside
      if (instance.getPlayerCount() > 0) {
         D.debug("Not going to destroy instance with active connections: " + instance.id);
         return;
      }

      // Make sure it exists in our mapping
      if (!_instances.ContainsKey(instance.id)) {
         D.debug("Instance Manager does not contain instance id: " + instance.id);
         return;
      }

      // Destroy any instance-specific entities
      foreach (NetworkBehaviour entity in instance.entities) {
         if (entity != null) {
            NetworkServer.Destroy(entity.gameObject);
         }
      }

      // If it was a pvp instance, remove it from the pvp manager's internal mapping
      if (instance.isPvP) {
         PvpManager.self.tryRemoveEmptyGame(instance.id);
      }

      // Remove it from our internal mapping
      if (CustomMapManager.isPrivateCustomArea(instance.areaKey)) {
         if (instance.areaKey.Contains(CustomFarmManager.GROUP_AREA_KEY)) {
            ServerNetworkingManager.self.releasePrivateFarmInstance(CustomMapManager.getUserId(instance.areaKey));
         } else if (instance.areaKey.Contains(CustomHouseManager.GROUP_AREA_KEY)) {
            ServerNetworkingManager.self.releasePrivateHouseInstance(CustomMapManager.getUserId(instance.areaKey));
         }
      }
      _instances.Remove(instance.id);

      // Remove the instance from the server network if it is a group instance
      ServerNetworkingManager.self.server.removeGroupInstance(instance);

      // Remove the instance from the server network
      ServerNetworkingManager.self.server.removeInstance(instance);

      // Then destroy the instance
      NetworkServer.Destroy(instance.gameObject);

      // Remove area game object if there are no instances using it
      if (WorldMapManager.isWorldMapArea(areaKey)) {
         List<Instance> allInstancesWithArea = _instances.Values.ToList().FindAll(_ => _.areaKey == areaKey);
         if (allInstancesWithArea.Count == 0) {
            Area areaReference = AreaManager.self.getArea(areaKey);
            if (areaReference != null) {
               Destroy(areaReference.gameObject);
               D.adminLog("Destroying Area Object: {" + areaKey + "}", D.ADMIN_LOG_TYPE.AreaClearing);
            }
            AreaManager.self.removeArea(areaKey);

            NetworkedServer netServerReference = ServerNetworkingManager.self.server;
            if (netServerReference != null) {
               if (netServerReference.areaBeingGenerated.ContainsKey(areaKey)) {
                  netServerReference.areaBeingGenerated.Remove(areaKey);
               }
               if (netServerReference.playerCountPerArea.ContainsKey(areaKey)) {
                  netServerReference.playerCountPerArea.Remove(areaKey);
               }
            }
         }
      }
   }

   public int getPlayerCountInInstance (int instanceId) {
      Instance instance = getInstance(instanceId);

      if (instance == null) {
         return 0;
      }

      int playerCount = instance.getPlayerCount();
      return playerCount;
   }

   public int getPlayerCountAllInstances () {
      int total = 0;
      foreach (KeyValuePair<int, Instance> pair in _instances) {
         total += getPlayerCountInInstance(pair.Key);
      }
      return total;
   }

   public void registerClientInstance (Instance instance) {
      if (!NetworkServer.active) {
         // Register or overwrite instance
         if (!_instances.ContainsKey(instance.id)) {
            _instances.Add(instance.id, instance);
         } else {
            _instances[instance.id] = instance;
         }

         // Cache all empty instances
         List<int> instanceToClear = new List<int>();
         foreach (KeyValuePair<int, Instance> existingInstance in _instances) {
            if (existingInstance.Value == null) {
               instanceToClear.Add(existingInstance.Key);
            }
         }

         // Remove all empty instances
         foreach (int instId in instanceToClear) {
            _instances.Remove(instId);
         }
      }
   }

   public List<InstanceOverview> createOverviewForAllInstances () {
      List<InstanceOverview> allInstances = new List<InstanceOverview>();
      foreach (Instance instance in _instances.Values) {
         if (!GroupInstanceManager.self.tryGetGroupInstance(instance.groupInstanceId, out GroupInstance groupInstance, true)) {
            groupInstance = new GroupInstance();
         }
         allInstances.Add(new InstanceOverview {
            id = instance.id,
            port = instance.serverPort,
            area = instance.areaKey,
            pCount = getPlayerCountInInstance(instance.id),
            maxPlayerCount = instance.getMaxPlayers(),
            groupInstance = groupInstance,
            difficulty = instance.difficulty,
            aliveEnemyCount = instance.aliveNPCEnemiesCount,
            totalEnemyCount = instance.getTotalNPCEnemyCount(),
            isPvp = instance.isPvP,
            creationDate = instance.creationDate,
            biome = instance.biome
         });
      }

      return allInstances;
   }

   #region Private Variables

   // The instance ID we're up to
   protected int _id = 1;

   // The instances we've created
   protected Dictionary<int, Instance> _instances = new Dictionary<int, Instance>();

   #endregion
}