﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Mirror;
using UnityEngine.EventSystems;
using System;
using static UnityEngine.UI.Dropdown;
using System.Text;
using System.Linq;
using TMPro;
using Steamworks;
using Steam;

public class OptionsPanel : Panel
{

   #region Public Variables

   // The zone Text
   public Text zoneText;

   // The size of the chat
   public TMP_Text chatSizeText;

   // The chat size slider
   public Slider chatSizeSlider;

   // The music slider
   public Slider musicSlider;

   // The effects slider
   public Slider effectsSlider;

   // The GUI scale slider
   public Slider guiScaleSlider;

   // The minimap scale slider
   public Slider minimapScaleSlider;

   // The label of the gui scale in percentage
   public TMP_Text guiScaleLabel;

   // The label of the minimap scale in percentage
   public TMP_Text minimapScaleLabel;

   // The available resolutions dropdown
   public TMP_Dropdown resolutionsDropdown;

   // The vsync toggle
   public Toggle vsyncToggle;

   // The button to apply changes to display settings
   public Button applyDisplaySettingsButton;

   // The guild icon toggle
   public Toggle displayGuildIconsToggle;

   // Flashing lights toggle
   public Toggle disableFlashingLightsToggle;

   // Player Name Display
   public Toggle displayPlayersNameToggle;

   // Help tips display
   public Toggle displayHelpTipsToggle;

   // Ignores guild alliance invites
   public Toggle ignoreGuildAllianceInviteToggle;

   // Reference to show heal text toggle
   public Toggle showHealTextToggle;

   // The constantly sprinting toggle
   public Toggle sprintConstantlyToggle;

   // A toggle controlling whether the user will automatically farm
   public Toggle autoFarmToggle;

   // Determines whether mouse cursor should be confined within the game screen
   public Toggle mouseLockToggle;

   // Determines whether the chat input remains focused after sending a message
   public Toggle chatPromptToggle;

   // Determines if soul binding warnings should be shown
   public Toggle showSoulbindingWarningsToggle;

   // A toggle controlling whether to enable the camera shake effect
   public Toggle cameraShakeToggle;

   // A toggle controlling whether to enable slow text effect
   public Toggle slowTextEffectToggle;

   // A toggle controlling whether to enable the camera shake effect
   public Toggle doNotDisturbToggle;

   // The screen mode toggle
   public TMP_Dropdown screenModeDropdown;

   // Bool to track if all players should continuously display their guild icon
   public static bool onlyShowGuildIconsOnMouseover = false;

   // Bool to track if all players should continuously display their name
   public static bool onlyShowPlayerNamesOnMouseover = false;

   // Bool to track if flashing lights should be disabled
   public static bool disableFlashingLights = false;

   // Self
   public static OptionsPanel self;

   // Version number gameObject
   public GameObject versionGameObject;

   // Version Number text field
   public TMP_Text versionNumberText;

   // The objects that only appears when a user is logged in
   public GameObject[] loggedInObjects;

   // The objects that only appears when a user is NOT logged in
   public GameObject[] notLoggedInObjects;

   // The objects that only appears when a user selected a character
   public GameObject[] hasCharacterObjects;

   // Buttons only admins can access
   public GameObject[] adminOnlyButtons;

   // If the initial option settings have been loaded
   public bool hasInitialized = false;

   // The single player toggle
   public Toggle singlePlayerToggle;

   // A reference to the game object containing the server log accessing buttons
   public GameObject serverLogRow;

   // A list of accepted fullscreen modes (windowed, borderless windowed, fullscreen)
   public List<FullScreenMode> fullScreenModes = new List<FullScreenMode>();

   // The label that displays the total amount of active players
   public TMP_Text activePlayersCountLabel;

   // Admin tooltip to display active players
   public ToolTipComponent activePlayersAdminTooltip;

   // Text indicating if we are using demo user or not
   public GameObject demoUserText = null;

   // Reference to invite link of Arcane Waters Discord game-talk channel
   public const string DISCORD_URL_INVITE = "https://discord.gg/arcanewaters";

   // Reference to Discord confirmation window title
   public const string DISCORD_CONFIRM_TITLE = "Opening Discord?";

   // Reference to Discord confirmation window description
   public const string DISCORD_CONFIRMATION_DESC = "You are about to open Arcane Waters Discord Channel. Are you sure?";

   // Reference to the Wishlist Button
   public Button wishlistButton;

   // Reference to tab active state
   public GameObject[] activeTab;

   // Reference to tab content panel
   public GameObject[] contentPanels;

   #endregion

   public override void Awake () {
      base.Awake();

      self = this;
   }

   public override void Start () {
      initializeResolutionsDropdown();
      initializeFullScreenSettings();

      musicSlider.value = SoundManager.musicVolume;
      effectsSlider.value = SoundManager.effectsVolume;
      chatSizeSlider.value = ChatManager.self.chatFontSize;
      chatSizeText.text = ChatManager.self.chatFontSize.ToString();
      chatSizeSlider.onValueChanged.AddListener(_ => {
         ChatManager.self.updateChatFontSize(_);
         chatSizeText.text = _.ToString();
      });

      // Loads the saved gui scale
      float guiScaleValue = OptionsManager.GUIScale;
      guiScaleLabel.text = Mathf.RoundToInt(guiScaleValue) + " %";
      guiScaleSlider.SetValueWithoutNotify(guiScaleValue / 25f);
      guiScaleSlider.onValueChanged.AddListener(_ => guiScaleSliderChanged());

      // Loads the saved minimap scale
      float minimapScaleValue = OptionsManager.minimapScale;
      minimapScaleLabel.text = Mathf.RoundToInt(minimapScaleValue) + " %";
      minimapScaleSlider.SetValueWithoutNotify(minimapScaleValue / 25f);
      minimapScaleSlider.onValueChanged.AddListener(_ => minimapScaleSliderChanged());

      // If any of the screen resolution settings is changed, we want to enable the button if the new setting is different from the applied one
      vsyncToggle.onValueChanged.AddListener((isOn) => applyDisplaySettingsButton.interactable = isOn != OptionsManager.isVsyncEnabled());
      screenModeDropdown.onValueChanged.AddListener((index) => {
         applyDisplaySettingsButton.interactable = index != getScreenModeIndex(ScreenSettingsManager.fullScreenMode);

         // In borderless fullscreen players can only use the native screen resolution
         bool isBorderlessWindow = index == getScreenModeIndex(FullScreenMode.FullScreenWindow);
         resolutionsDropdown.interactable = !isBorderlessWindow;

         if (isBorderlessWindow) {
            int width = Screen.currentResolution.width;
            int height = Screen.currentResolution.height;
            resolutionsDropdown.SetValueWithoutNotify(getResolutionOptionIndex(width, height, getMaxRefreshRateForResolution(width, height)));
         }
      });

      resolutionsDropdown.onValueChanged.AddListener((index) => applyDisplaySettingsButton.interactable = index != getActiveResolutionOptionIndex());

      // Loads vsync
      int vsyncCount = OptionsManager.vsyncCount;
      vsyncToggle.SetIsOnWithoutNotify(vsyncCount != 0);
      QualitySettings.vSyncCount = vsyncCount;
      ignoreGuildAllianceInviteToggle.isOn = Global.ignoreGuildAllianceInvites;
      ignoreGuildAllianceInviteToggle.onValueChanged.AddListener(_ => {
         Global.ignoreGuildAllianceInvites = _;
         PlayerPrefs.SetInt(OptionsManager.PREF_GUILD_ALLIANCE_INVITE, _ == true ? 1 : 0);
      });

      // Set the single player toggle event
      singlePlayerToggle.onValueChanged.AddListener(_ => {
         Global.isSinglePlayer = _;
      });

      // Set the guild icons toggle event
      displayGuildIconsToggle.onValueChanged.AddListener(value => {
         onlyShowGuildIconsOnMouseover = !value;
         showAllGuildIcons(value);
      });

      // Set the flashing lights toggle event
      disableFlashingLightsToggle.onValueChanged.AddListener(value => {
         disableFlashingLights = value;
      });

      // Set the player name toggle event
      displayPlayersNameToggle.onValueChanged.AddListener(value => {
         onlyShowPlayerNamesOnMouseover = !value;
         showPlayersName(value);
      });

#if UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
      mouseLockToggle.transform.parent.gameObject.SetActive(false);
#else
      // Set the player name toggle event
      mouseLockToggle.isOn = PlayerPrefs.GetInt(OptionsManager.PREF_LOCK_CURSOR) == 1 ? true : false;
      ScreenSettingsManager.self.refreshMouseLockState();
      mouseLockToggle.onValueChanged.AddListener(value => {
         PlayerPrefs.SetInt(OptionsManager.PREF_LOCK_CURSOR, value ? 1 : 0);
      });
#endif

      // Initialize the help tips toggle
      displayHelpTipsToggle.SetIsOnWithoutNotify(!NotificationManager.self.areAllNotificationsDisabled());
      displayHelpTipsToggle.onValueChanged.AddListener(value => {
         setHelpTipsDisplay();
      });

      sprintConstantlyToggle.isOn = PlayerPrefs.GetInt(OptionsManager.PREF_SPRINT_CONSTANTLY) == 1 ? true : false;
      sprintConstantlyToggle.onValueChanged.AddListener(value => {
         PlayerPrefs.SetInt(OptionsManager.PREF_SPRINT_CONSTANTLY, value ? 1 : 0);
         Global.sprintConstantly = value;
      });

      bool autoFarm = PlayerPrefs.GetInt(OptionsManager.PREF_AUTO_FARM, 0) == 1 ? true : false;
      autoFarmToggle.isOn = autoFarm;
      autoFarmToggle.onValueChanged.AddListener(value => {
         PlayerPrefs.SetInt(OptionsManager.PREF_AUTO_FARM, value ? 1 : 0);
         Global.autoFarm = value;
      });

      bool showHealText = Global.showHealText;
      showHealTextToggle.isOn = showHealText;
      Global.showHealText = showHealText;
      showHealTextToggle.onValueChanged.AddListener(value => {
         PlayerPrefs.SetInt(OptionsManager.SHOW_HEAL_TEXT, value ? 1 : 0);
         Global.showHealText = value;
      });

      bool chatInputRemainsFocused = PlayerPrefs.GetInt(OptionsManager.PREF_CHAT_INPUT_REMAINS_FOCUSED, 0) == 1 ? true : false;
      chatPromptToggle.isOn = chatInputRemainsFocused;
      Global.chatInputRemainsFocused = chatInputRemainsFocused;
      chatPromptToggle.onValueChanged.AddListener(value => {
         PlayerPrefs.SetInt(OptionsManager.PREF_CHAT_INPUT_REMAINS_FOCUSED, value ? 1 : 0);
         Global.chatInputRemainsFocused = value;
      });

      bool showSoulbinding = PlayerPrefs.GetInt(OptionsManager.PREF_SHOW_SOUL_BINDING_WARNINGS, 1) == 1 ? true : false;
      showSoulbindingWarningsToggle.isOn = showSoulbinding;
      Global.showSoulbindingWarnings = showSoulbinding;
      showSoulbindingWarningsToggle.onValueChanged.AddListener(value => {
         PlayerPrefs.SetInt(OptionsManager.PREF_SHOW_SOUL_BINDING_WARNINGS, value ? 1 : 0);
         Global.showSoulbindingWarnings = value;
      });

      bool enableCameraShakeEffect = PlayerPrefs.GetInt(OptionsManager.PREF_ENABLE_CAMERA_SHAKE, 1) == 1 ? true : false;
      cameraShakeToggle.isOn = enableCameraShakeEffect;
      Global.isCameraShakeEffectEnabled = enableCameraShakeEffect;
      cameraShakeToggle.onValueChanged.RemoveAllListeners();
      cameraShakeToggle.onValueChanged.AddListener(value => {
         PlayerPrefs.SetInt(OptionsManager.PREF_ENABLE_CAMERA_SHAKE, value ? 1 : 0);
         Global.isCameraShakeEffectEnabled = value;
      });

      bool slowTextEnable = PlayerPrefs.GetInt(OptionsManager.PREF_ENABLE_SLOW_TEXT, 1) == 1 ? true : false;
      slowTextEffectToggle.isOn = slowTextEnable;
      Global.slowTextEnabled = slowTextEnable;
      slowTextEffectToggle.onValueChanged.RemoveAllListeners();
      slowTextEffectToggle.onValueChanged.AddListener(value => {
         PlayerPrefs.SetInt(OptionsManager.PREF_ENABLE_SLOW_TEXT, value ? 1 : 0);
         Global.slowTextEnabled = value;
      });

      bool doNotDisturbEnable = PlayerPrefs.GetInt(OptionsManager.PREF_DO_NOT_DISTURB, 0) == 1 ? true : false;
      doNotDisturbToggle.isOn = doNotDisturbEnable;
      Global.doNotDisturbEnabled = doNotDisturbEnable;
      doNotDisturbToggle.onValueChanged.RemoveAllListeners();
      doNotDisturbToggle.onValueChanged.AddListener(value => {
         PlayerPrefs.SetInt(OptionsManager.PREF_DO_NOT_DISTURB, value ? 1 : 0);
         Global.doNotDisturbEnabled = value;
      });

      // Build string and show version number
      versionGameObject.SetActive(true);
      versionNumberText.text = Util.getFormattedGameVersion();

      applyDisplaySettingsButton.onClick.RemoveAllListeners();
      applyDisplaySettingsButton.onClick.AddListener(() => applyDisplaySettings());

      refreshDisplaySettingsControls();

      requestPlayersCount();

      activePlayersAdminTooltip.message = "";
   }

   public void OnEnable () {
      // Enable first tab when option is enabled
      onTabClicked(0);
   }

   public void onTabClicked (int index) {
      // We must disable any active tab before enable active state of selected tab 
      foreach (var tab in activeTab) {
         tab.SetActive(false);
      }

      // We must disable any active panel before enabling panel of active tab
      foreach (var panel in contentPanels) {
         panel.SetActive(false);
      }

      activeTab[index].SetActive(true);
      contentPanels[index].SetActive(true);
   }

   public void showAllGuildIcons (bool showGuildIcons) {
      if (showGuildIcons) {
         // Display the guild icons of all the players
         foreach (NetEntity entity in EntityManager.self.getAllEntities()) {
            if ((entity.guildId > 0) && (entity is PlayerBodyEntity)) {
               entity.updateGuildIconSprites();
               entity.showGuildIcon();
            }
         }
      } else {
         // Do not display the guild icons of all the players
         foreach (NetEntity entity in EntityManager.self.getAllEntities()) {
            if (entity is PlayerBodyEntity) {
               entity.hideGuildIcon();
            }
         }
      }
   }

   public void showPlayersName (bool displayPlayersName) {
      if (displayPlayersName) {
         foreach (NetEntity entity in EntityManager.self.getAllEntities()) {
            if (entity is PlayerBodyEntity) {
               entity.showEntityName();
            }
         }
      } else {
         foreach (NetEntity entity in EntityManager.self.getAllEntities()) {
            if (entity is PlayerBodyEntity) {
               entity.hideEntityName();
            }
         }
      }
   }

   public void setHelpTipsDisplay () {
      NotificationManager.self.toggleNotifications(displayHelpTipsToggle.isOn);
   }

   public void setVSync (bool vsync) {
      OptionsManager.setVsync(vsync);

      if (vsyncToggle.isOn != vsync) {
         vsyncToggle.SetIsOnWithoutNotify(vsync);
      }
   }

   private void initializeFullScreenSettings () {
      List<TMP_Dropdown.OptionData> screenModeOptions = new List<TMP_Dropdown.OptionData>();

      // Initialize override options
      initializeFullScreenModesList();

      // Initialize display options
      screenModeOptions.Add(new TMP_Dropdown.OptionData { text = "Fullscreen" });
      screenModeOptions.Add(new TMP_Dropdown.OptionData { text = "Borderless" });
      screenModeOptions.Add(new TMP_Dropdown.OptionData { text = "Windowed" });

      screenModeDropdown.options = screenModeOptions;
   }

   public int getScreenModeIndex (FullScreenMode mode) {
      if (fullScreenModes == null || fullScreenModes.Count < 1) {
         initializeFullScreenModesList();
      }

      return fullScreenModes.IndexOf(mode);
   }

   private void initializeFullScreenModesList () {
      fullScreenModes = new List<FullScreenMode>();
      fullScreenModes.Add(FullScreenMode.ExclusiveFullScreen);
      fullScreenModes.Add(FullScreenMode.FullScreenWindow);
      fullScreenModes.Add(FullScreenMode.Windowed);
   }

   private void initializeResolutionsList () {
      Resolution[] allResolutions = Screen.resolutions;

      // Remove unsupported and duplicate resolutions (duplicate with different refresh rates)
      _supportedResolutions = new List<Resolution>();
      for (int i = 0; i < allResolutions.Length; i++) {
         Resolution res = allResolutions[i];

         if (res.width >= res.height &&
            res.width >= ScreenSettingsManager.MIN_WIDTH &&
            res.height >= ScreenSettingsManager.MIN_HEIGHT &&
            !_supportedResolutions.Exists(r => r.width == res.width &&
               r.height == res.height && r.refreshRate == res.refreshRate)) {
            _supportedResolutions.Add(res);
         }
      }
   }

   private void initializeResolutionsDropdown () {
      initializeResolutionsList();

      List<TMP_Dropdown.OptionData> options = new List<TMP_Dropdown.OptionData>();
      int currentResolution = -1;

      for (int i = 0; i < _supportedResolutions.Count; i++) {
         Resolution res = _supportedResolutions[i];
         TMP_Dropdown.OptionData o = new TMP_Dropdown.OptionData($"{res.width} x {res.height} ({res.refreshRate})");
         options.Add(o);

         if (res.width == ScreenSettingsManager.width && res.height == ScreenSettingsManager.height) {
            currentResolution = i;
         }
      }

      resolutionsDropdown.options = options;

      if (currentResolution >= 0) {
         resolutionsDropdown.SetValueWithoutNotify(currentResolution);
      }
   }

   private int getActiveResolutionOptionIndex () {
      return getResolutionOptionIndex(ScreenSettingsManager.width, ScreenSettingsManager.height, ScreenSettingsManager.refreshRate);
   }

   private int getResolutionOptionIndex (int width, int height, int refreshRate) {
      return _supportedResolutions.FindIndex(x => x.width == width && x.height == height && x.refreshRate == refreshRate);
   }

   private void setResolution (int resolutionIndex) {
      ScreenSettingsManager.setResolution(_supportedResolutions[resolutionIndex].width, _supportedResolutions[resolutionIndex].height, _supportedResolutions[resolutionIndex].refreshRate);
   }

   private int getMaxRefreshRateForResolution (int width, int height) {
      if (_supportedResolutions == null || _supportedResolutions.Count < 1) {
         initializeResolutionsList();
      }

      if (_supportedResolutions.Any(x => x.width == width && x.height == height)) {
         return _supportedResolutions.Where(x => x.width == width && x.height == height).Max(r => r.refreshRate);
      } else {
         D.warning($"Could not find max refresh rate for resolution {width} x {height}. Returning default.");
         return 59;
      }
   }

   private void refreshDisplaySettingsControls () {
      int vsyncCount = OptionsManager.vsyncCount;
      vsyncToggle.SetIsOnWithoutNotify(vsyncCount != 0);

      int loadedScreenModeValue = getScreenModeIndex(ScreenSettingsManager.fullScreenMode);
      screenModeDropdown.SetValueWithoutNotify(loadedScreenModeValue);

      int activeResolutionIndex = -1;
      if (ScreenSettingsManager.fullScreenMode == FullScreenMode.FullScreenWindow) {
         int width = Screen.currentResolution.width;
         int height = Screen.currentResolution.height;

         activeResolutionIndex = getResolutionOptionIndex(width, height, getMaxRefreshRateForResolution(width, height));
         resolutionsDropdown.interactable = false;
      } else {
         activeResolutionIndex = getActiveResolutionOptionIndex();
         resolutionsDropdown.interactable = true;
      }

      if (activeResolutionIndex >= 0) {
         resolutionsDropdown.SetValueWithoutNotify(activeResolutionIndex);
      }

      // The controls now match the applied settings, we can disable the "Apply" button until something changes again
      applyDisplaySettingsButton.interactable = false;
   }

   private void applyDisplaySettings () {
      setResolution(resolutionsDropdown.value);
      setVSync(vsyncToggle.isOn);
      ScreenSettingsManager.setFullscreenMode(fullScreenModes[screenModeDropdown.value]);

      refreshDisplaySettingsControls();
   }

   public override void show () {
      base.show();

      // Show/hide some options when the user is logged in and when he is not
      bool isLoggedIn = NetworkServer.active || NetworkClient.active;
      foreach (GameObject options in loggedInObjects) {
         options.SetActive(isLoggedIn);
      }
      foreach (GameObject options in notLoggedInObjects) {
         options.SetActive(!isLoggedIn);
      }

      // Show/hide some options when player has/hasn't selected a character
      bool hasCharacter = Global.player != null;
      foreach (GameObject options in hasCharacterObjects) {
         options.SetActive(hasCharacter);
      }

      bool isAdmin = Global.isLoggedInAsAdmin();
      serverLogRow.SetActive(isAdmin);
      if (isAdmin) {
         Global.player.admin.Cmd_GetServerLogString();
      }

      demoUserText.SetActive(Global.player != null && Global.player.isDemoUser);

      refreshDisplaySettingsControls();

      requestPlayersCount();

      ScreenSettingsManager.self.refreshMouseLockState();

      activePlayersAdminTooltip.gameObject.SetActive(isAdmin);

      toggleWishlistButton();
   }

   public override void hide () {
      base.hide();
      ScreenSettingsManager.self.refreshMouseLockState();
   }

   private void requestPlayersCount () {
      if (Global.player != null) {
         Global.player.rpc.Cmd_RequestPlayersCount(Global.player.isAdmin());
      }
   }

   public void receiveDataFromServer (int instanceNumber, int totalInstances) {
      // Note that we just started being shown
      _lastShownTime = Time.time;

      // Update the Zone info
      if (zoneText) {
         zoneText.text = "Zone " + instanceNumber + " of " + totalInstances;
      }

      // Set our music and volume sliders
      musicSlider.value = SoundManager.musicVolume / 1f;
      effectsSlider.value = SoundManager.effectsVolume / 1f;
   }

   public void musicSliderChanged () {
      // If the panel just switched on, ignore the change
      if (Time.time - _lastShownTime < .1f) {
         return;
      }

      SoundManager.musicVolume = musicSlider.value;

      SoundManager.self.musicVCA.setVolume(SoundManager.musicVolume);
   }

   public void effectsSliderChanged () {
      // If the panel just switched on, ignore the change
      if (Time.time - _lastShownTime < .1f) {
         return;
      }

      SoundManager.effectsVolume = effectsSlider.value;

      SoundManager.self.sfxVCA.setVolume(SoundManager.effectsVolume);
   }

   public void guiScaleSliderChanged () {
      guiScaleLabel.text = Mathf.RoundToInt(guiScaleSlider.value * 25) + " %";
   }

   public void applyGuiScaleChanges () {
      OptionsManager.setGUIScale(Mathf.RoundToInt(guiScaleSlider.value * 25));
   }

   public void minimapScaleSliderChanged () {
      int scale = Mathf.RoundToInt(minimapScaleSlider.value * 25);
      minimapScaleLabel.text = scale + " %";
      OptionsManager.setMinimapScale(scale);
   }

   public void onOpenLogFilePressed () {
      D.openLogFile();
   }

   public void onCopyLogPressed () {
      D.copyLogToClipboard();
   }

   public void onOpenServerLogFilePressed () {
      D.openServerLogFile();
   }

   public void onCopyServerLogFilePressed () {
      D.copyServerLogToClipboard();
   }

   public void onLogOutButtonPress () {
      if (Global.player == null) {
         // If we are at the character screen, lets go back to title
         if (CharacterScreen.self.isShowing()) {
            // Return to the title screen
            Util.stopHostAndReturnToTitleScreen();

            if (CharacterCreationPanel.self.isShowing()) {
               CharacterCreationPanel.self.cancelCreating();
            }

            // Close this panel
            if (isShowing()) {
               PanelManager.self.hideCurrentPanel();
            }
         }

         return;
      }

      // Hide the group invite panel, if opened
      GroupManager.self.refuseGroupInvitation();

      // Stop weather simulation
      WeatherManager.self.setWeatherSimulation(WeatherEffectType.None);

      // Check if the user is at sea
      if (Global.player is ShipEntity) {
         // Initialize the countdown screen
         PanelManager.self.countdownScreen.cancelButton.onClick.RemoveAllListeners();
         PanelManager.self.countdownScreen.onCountdownEndEvent.RemoveAllListeners();
         PanelManager.self.countdownScreen.cancelButton.onClick.AddListener(() => PanelManager.self.countdownScreen.hide());
         PanelManager.self.countdownScreen.onCountdownEndEvent.AddListener(() => logOut());
         PanelManager.self.countdownScreen.customText.text = "Logging out in";

         // Start the countdown
         PanelManager.self.countdownScreen.seconds = DisconnectionManager.SECONDS_UNTIL_PLAYERS_DESTROYED;
         PanelManager.self.countdownScreen.show();

         // Close this panel
         PanelManager.self.hideCurrentPanel();
      } else {
         logOut();
      }
   }

   public void onGoHomeButtonPress () {
      if (Global.player == null) {
         return;
      }

      Global.player.Cmd_GoHome();

      // Close this panel
      if (isShowing()) {
         PanelManager.self.hideCurrentPanel();
      }
   }

   public void onTutorialButtonPress () {
      TutorialManager3.self.panel.openPanel();
   }

   public void onKeybindingsButtonPress () {
      PanelManager.self.showPanel(Type.Keybindings);
   }

   public void onGifButtonPress () {
      PanelManager.self.showPanel(Type.GIFReplaySettings);
   }

   public void onExitButtonPress () {
      PanelManager.self.showConfirmationPanel("Are you sure you want to exit the game?",
         () => {
            Application.Quit();
         });
   }

   public void onHelpButtonPress () {
      if (PanelManager.self == null) {
         return;
      }

      PanelManager.self.showPanel(Panel.Type.Help);
   }

   public void logOut () {
      if (Global.player == null) {
         return;
      }

      // Hide the group invite panel, if opened
      GroupManager.self.refuseGroupInvitation();

      // Tell the server that the player logged out safely
      Global.player.rpc.Cmd_OnPlayerLogOutSafely();

      // Close this panel
      if (isShowing()) {
         PanelManager.self.hideCurrentPanel();
      }

      LoadingUtil.executeAfterFade(() => {
         // Return to the character selection screen
         Util.stopHostAndReturnToCharacterSelectionScreen();
      });
   }

   public void enableAdminButtons (bool isEnabled) {
      foreach (GameObject row in adminOnlyButtons) {
         row.SetActive(isEnabled);
      }
   }

   public void onPlayersCountReceived (int playersCount, string[] playersNames) {
      activePlayersCountLabel.text = "Active Players: " + playersCount.ToString();
      activePlayersAdminTooltip.message = string.Join("\n", playersNames);
   }

   public void onWishlistButtonPress () {
      // The player must be logged into Steam
      if (Global.player == null || !SteamManager.Initialized) {
         PanelManager.self.noticeScreen.show("Can't open the Wishlist page at the moment.");
         return;
      }

      D.debug("Wishlist Request Received. Opening Overlay.");
      SteamFriends.ActivateGameOverlayToStore(new AppId_t(uint.Parse(SteamStatics.GAME_APPID)), EOverlayToStoreFlag.k_EOverlayToStoreFlag_None);

      D.debug("Wishlist Request Received. Sent to Server.");
      Global.player.rpc.Cmd_NotePlayerWishlist(SteamUtils.GetAppID().ToString());
   }

   public void onDiscordButtonPress () {
      // Open discord game-talk channel upon player confirmation
      PanelManager.self.showConfirmationPanel(DISCORD_CONFIRM_TITLE, onConfirm: () => Application.OpenURL(DISCORD_URL_INVITE), description: DISCORD_CONFIRMATION_DESC);
   }

   private void toggleWishlistButton () {
      if (wishlistButton == null) {
         return;
      }

      // Activate the button if the player has logged in through Steam
      wishlistButton.interactable = SteamManager.Initialized;
   }

   #region Private Variables

   // The time at which we were last shown
   protected float _lastShownTime;

   // The list of supported resolutions
   private List<Resolution> _supportedResolutions = new List<Resolution>();

   // Reference to Wishlist window unavailable description
   private const string WISHLIST_UNAVAILABLE = "Can't open the Wishlist page at the moment.";

   #endregion
}
