﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Mirror;
using UnityEngine.EventSystems;
using System;
using static UnityEngine.UI.Dropdown;
using System.Text;
using System.Linq;
using TMPro;
using UnityEngine.Events;

public class AdminPanel : Panel
{
   #region Public Variables

   // The slider that allows to set the Players Count Limit on a server
   public Slider sliderPlayersCountMax;

   // The input field that allows to set the Players Count Limit on a server
   public TMP_InputField txtPlayersCountMax;

   // The toggle that sets the Admin Only flag
   public Toggle toggleAdminOnlyMode;

   // The reference to the input field that displays the message shown to users when the server is in Admin Only mode
   public TMP_InputField txtAdminOnlyModeMessage;

   // The reference to the Apply button
   public Button btnApply;

   // The reference to the Refresh button
   public Button btnRefresh;

   // The message to display when kicking all users from the servers
   public TMP_InputField txtKickAllUsersMessage;

   // The reference to the load blocker
   public GameObject loadBlocker;

   // Reference to the Tabs
   public AdminPanelTabs tabs;

   // Reference to the Tabs Switcher
   public AdminPanelTabSwitcher tabSwitcher;

   // Reference to the Metric Holder
   public AdminPanelMetricsHolder metricsHolder;

   // Text that displays how many servers we are fetching
   public Text serversTitleText = null;

   // The toggle that sets whether the Gem Store should be accessible
   public Toggle toggleEnableGemStore;

   // The reference to the input field that displays the message shown to users when the Gem Store is disabled
   public TMP_InputField txtGemStoreDisabledMessage;

   // Self
   public static AdminPanel self;

   #endregion

   public override void Awake () {
      base.Awake();
      self = this;

      initializeTabs();
   }

   public override void Start () {
      sliderPlayersCountMax.onValueChanged.RemoveAllListeners();
      sliderPlayersCountMax.onValueChanged.AddListener(onSliderPlayersCountMaxValueChanged);

      txtPlayersCountMax.onValueChanged.RemoveAllListeners();
      txtPlayersCountMax.onValueChanged.AddListener(onTxtPlayersCountMaxValueChanged);
   }

   public new void show () {
      if (!Global.isLoggedInAsAdmin()) {
         return;
      }

      if (!isShowing()) {
         btnApply.interactable = true;
         btnRefresh.interactable = true;
         toggleBlocker(true);
         requestRemoteSettings();

         refreshMetrics();
      }

      PanelManager.self.showPanel(Panel.Type.Admin);
   }

   private void requestRemoteSettings () {
      Global.player.rpc.Cmd_RequestRemoteSettings(
         new string[] {
                  RemoteSettingsManager.SettingNames.PLAYERS_COUNT_MAX,
                  RemoteSettingsManager.SettingNames.ADMIN_ONLY_MODE,
                  RemoteSettingsManager.SettingNames.ADMIN_ONLY_MODE_MESSAGE,
                  RemoteSettingsManager.SettingNames.ENABLE_GEM_STORE,
                  RemoteSettingsManager.SettingNames.GEM_STORE_DISABLED_MESSAGE
         });
   }

   private void toggleBlocker (bool show) {
      if (loadBlocker != null) {
         loadBlocker.gameObject.SetActive(show);
      }
   }

   public void onRemoteSettingsReceived (RemoteSettingCollection collection) {
      if (!Global.isLoggedInAsAdmin()) {
         return;
      }

      sliderPlayersCountMax.value = collection.getSetting(RemoteSettingsManager.SettingNames.PLAYERS_COUNT_MAX).toInt();
      toggleAdminOnlyMode.SetIsOnWithoutNotify(collection.getSetting(RemoteSettingsManager.SettingNames.ADMIN_ONLY_MODE).toBool());
      txtAdminOnlyModeMessage.text = collection.getSetting(RemoteSettingsManager.SettingNames.ADMIN_ONLY_MODE_MESSAGE).value;
      toggleEnableGemStore.SetIsOnWithoutNotify(collection.getSetting(RemoteSettingsManager.SettingNames.ENABLE_GEM_STORE).toBool());
      txtGemStoreDisabledMessage.text = collection.getSetting(RemoteSettingsManager.SettingNames.GEM_STORE_DISABLED_MESSAGE).value;

      toggleBlocker(false);
   }

   public void receiveServerCount (int count) {
      // Here we receive the number of servers we have in the network, so we know for how many to wait
      if (!Global.isLoggedInAsAdmin()) {
         return;
      }

      _totalServersInNetwork = count;
      _serverOverviews.Clear();

      toggleBlocker(false);
      btnRefresh.interactable = true;
      metricsHolder.clearMetrics();

      serversTitleText.text = "SERVERS 0/" + count;
   }

   public void receiveServerOverview (ServerOverview overview) {
      // Here we receive overview about one of the servers, add an entry for it in the panel
      if (!Global.isLoggedInAsAdmin()) {
         return;
      }

      // If we have this server already, ignore
      foreach (ServerOverview ov in _serverOverviews) {
         if (ov.serverNetworkId == overview.serverNetworkId) {
            return;
         }
      }

      _serverOverviews.Add(overview);
      metricsHolder.addServerOverview(_serverOverviews, overview);

      serversTitleText.text = "SERVERS " + _serverOverviews.Count + "/" + _totalServersInNetwork;
   }

   public void receiveServerLog (int targetServerPort, string log) {
      foreach (AdminPanelServerOverview ov in GetComponentsInChildren<AdminPanelServerOverview>()) {
         if (ov.data != null && ov.data.port == targetServerPort) {
            ov.receiveLog(log);
         }
      }
   }

   public void onApplyButtonPressed () {
      // Disable Apply Button
      btnApply.interactable = false;

      // Show confirmation screen
      PanelManager.self.confirmScreen.confirmButton.onClick.RemoveAllListeners();
      PanelManager.self.confirmScreen.confirmButton.onClick.AddListener(onApplyConfirmPressed);
      PanelManager.self.confirmScreen.cancelButton.onClick.RemoveAllListeners();
      PanelManager.self.confirmScreen.cancelButton.onClick.AddListener(onApplyCancelPressed);
      PanelManager.self.confirmScreen.show("Apply Changes", newDescription: "Do you really want to apply your changes?");
   }

   private void onApplyConfirmPressed () {
      if (!Global.isLoggedInAsAdmin()) {
         close();
      }

      // Disable confirm and cancel button
      PanelManager.self.confirmScreen.confirmButton.interactable = false;
      PanelManager.self.confirmScreen.cancelButton.interactable = false;

      // Update the remote settings
      int newPlayersCountMax = Mathf.FloorToInt(sliderPlayersCountMax.value);
      bool newIsAdminOnlyModeEnabled = toggleAdminOnlyMode.isOn;
      string newAdminOnlyModeMessage = txtAdminOnlyModeMessage.text;
      bool newIsGemStoreEnabled = toggleEnableGemStore.isOn;
      string newGemStoreDisabledMessage = txtGemStoreDisabledMessage.text;

      RemoteSettingCollection collection = new RemoteSettingCollection();
      collection.addSetting(RemoteSetting.create(RemoteSettingsManager.SettingNames.PLAYERS_COUNT_MAX, newPlayersCountMax.ToString(), settingValueType: RemoteSetting.RemoteSettingValueType.INT));
      collection.addSetting(RemoteSetting.create(RemoteSettingsManager.SettingNames.ADMIN_ONLY_MODE, newIsAdminOnlyModeEnabled.ToString(), settingValueType: RemoteSetting.RemoteSettingValueType.STRING));
      collection.addSetting(RemoteSetting.create(RemoteSettingsManager.SettingNames.ADMIN_ONLY_MODE_MESSAGE, newAdminOnlyModeMessage.ToString(), settingValueType: RemoteSetting.RemoteSettingValueType.STRING));
      collection.addSetting(RemoteSetting.create(RemoteSettingsManager.SettingNames.ENABLE_GEM_STORE, newIsGemStoreEnabled.ToString(), settingValueType: RemoteSetting.RemoteSettingValueType.BOOL));
      collection.addSetting(RemoteSetting.create(RemoteSettingsManager.SettingNames.GEM_STORE_DISABLED_MESSAGE, newGemStoreDisabledMessage.ToString(), settingValueType: RemoteSetting.RemoteSettingValueType.STRING));

      Global.player.rpc.Cmd_SetRemoteSettings(collection);
   }

   private void onApplyCancelPressed () {
      btnApply.interactable = true;
   }

   public void onSetRemoteSettings (bool success) {
      // Re-enable the confirm button before hiding the confirmation screen
      PanelManager.self.confirmScreen.confirmButton.interactable = true;
      PanelManager.self.confirmScreen.cancelButton.interactable = true;
      PanelManager.self.confirmScreen.hide();

      // Notify the user
      if (success) {
         PanelManager.self.noticeScreen.show("Changes Applied Successfully!");
      } else {
         PanelManager.self.noticeScreen.show("Error");
      }

      btnApply.interactable = true;
   }

   public void refreshMetrics () {
      // Disable Refresh button
      btnRefresh.interactable = false;

      // Show loading blocker
      toggleBlocker(true);

      // Request network overview
      Global.player.rpc.Cmd_RequestNetworkOverview();
   }

   private void onTxtPlayersCountMaxValueChanged (string newText) {
      // Validate the new value
      if (int.TryParse(newText, out int newValue)) {
         newText = Mathf.Max(newValue, 0).ToString();
      } else {
         newText = "0";
      }

      sliderPlayersCountMax.value = int.Parse(newText);
   }

   private void onSliderPlayersCountMaxValueChanged (float newValue) {
      txtPlayersCountMax.SetTextWithoutNotify(Mathf.FloorToInt(newValue).ToString());
   }

   private void initializeTabs () {
      if (tabs == null) {
         return;
      }

      tabs.onTabPressed.RemoveAllListeners();
      tabs.onTabPressed.AddListener(onTabPressed);

      // Switch to the first tab
      tabs.performTabPressed(0);
   }

   private void onTabPressed (int tabIndex) {
      if (tabSwitcher == null) {
         return;
      }

      tabSwitcher.performSwitch(tabIndex);
   }

   public void onKickAllNonAdminUsersButtonPressed () {
      // Deactivate "Confirm" Button until admin types in the word "Confirm" into the inputfield
      PanelManager.self.confirmScreen.enableConfirmInputField("CONFIRM");

      // Ask the admin for confirmation.
      PanelManager.self.showConfirmationPanel("Are you sure you want to kick all non-admins from the server?", () => onConfirmKickAllNonAdminUsers());
   }

   private void onConfirmKickAllNonAdminUsers () {
      Global.player.rpc.Cmd_RequestKickAllNonAdminUsers(txtKickAllUsersMessage.text);
   }

   #region Private Variables

   // How many server are in the network
   private int _totalServersInNetwork = 0;

   // Server overviews that we know about
   private List<ServerOverview> _serverOverviews = new List<ServerOverview>();

   #endregion
}
