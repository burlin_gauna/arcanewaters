﻿using UnityEngine;

public struct WorldMapAreaCoords
{
   #region Public Variables

   // X coordinate
   public int x;

   // Y coordinate
   public int y;

   #endregion

   public WorldMapAreaCoords (int x, int y) {
      this.x = x;
      this.y = y;
   }
}
