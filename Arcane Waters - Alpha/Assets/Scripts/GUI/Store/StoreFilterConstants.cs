﻿public class StoreFilterConstants
{
   #region Public Variables

   // The 'All' value
   public const string ALL = "All";

   // The 'Primary' value
   public const string PRIMARY = "Primary";

   // The 'Secondary' value
   public const string SECONDARY = "Secondary";

   // The 'Accent' value
   public const string ACCENT = "Accent";

   #endregion

   #region Private Variables

   #endregion
}
