﻿public struct HatDyeSupportInfo
{
   #region Public Variables

   // Does a hat support primary dyes?
   public bool supportsPrimaryDyes;

   // Does a hat support secondary dyes?
   public bool supportsSecondaryDyes;

   // Does a hat support accent dyes?
   public bool supportsAccentDyes;

   #endregion
}

