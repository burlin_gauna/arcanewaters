﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Mirror;

public class TradeChatLine : ChatLine {
   #region Public Variables

   // The various components we manage
   public Text buySellText;
   public Text countText;
   public Text priceText;
   public Image cargoIcon;

   #endregion

   #region Private Variables

   #endregion
}
