﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Mirror;

public class FlagShipAbilityRow : MonoBehaviour {
   #region Public Variables

   // Image component
   public Image iconImage;

   // Text displaying name
   public Text abilityNameText;

   // String data
   public string abilityName;
   public string abilityInfo;

   // The ship ability data reference
   public ShipAbilityData shipAbilityData;

   // The ability name holder
   public GameObject abilityNameHolder;

   #endregion

   public void pointerEnter () {
      FlagshipPanel panel = (FlagshipPanel) PanelManager.self.get(Panel.Type.Flagship);
      panel.shipAbilityTooltip.triggerAbilityTooltip(transform.position, shipAbilityData);
   }

   public void pointerExit () {
      FlagshipPanel panel = (FlagshipPanel) PanelManager.self.get(Panel.Type.Flagship);
      panel.shipAbilityTooltip.abilityToolTipHolder.SetActive(false);
   }

   #region Private Variables

   #endregion
}
