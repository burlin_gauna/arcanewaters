using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class DamageText : MonoBehaviour {
   #region Public Variables

   // The amount of time it takes for the damage text to fade out completely
   public static float TEXT_LIFETIME = 1.3f;

   // The amount of time it takes the text to reach full size
   public static float SIZE_INCREASE_DURATION = .10f;

   // The speed to reach full text size
   public static float SIZE_INCREASE_SPEED = 4;

   // The Text component
   public Text text;

   // The Image component
   public Image iconImage;

   // The prefab we use for UI collisions
   public GameObject uiColliderPrefab;

   #endregion

   void Awake () {
      _creationTime = Time.time;

      // Look up the row that contains the text and image
      _row = GetComponentInChildren<HorizontalLayoutGroup>();

      // Start with the text completely scaled down
      _row.transform.localScale = new Vector3(0f, 0f, 1f);
   }

   void Start () {
      // Create a UI collider beneath us to bounce off of
      _uiCollider = (GameObject) GameObject.Instantiate(uiColliderPrefab);
      _uiCollider.name = "UI Collider";

      // Position us beneath the damage text so that it will fall and bounce
      _uiCollider.transform.SetParent(EffectManager.self.transform);
      _uiCollider.transform.position = new Vector3(
          this.transform.position.x,
          this.transform.position.y - .3f,
          this.transform.position.z
      );

      // The collider should be on the same layer as the damage text
      _uiCollider.layer = this.gameObject.layer;
   }

   public void Update () {
      float timeSinceCreation = Time.time - _creationTime;

      // Destroy ourself after enough time has passed
      if (timeSinceCreation > TEXT_LIFETIME) {
         Destroy(_uiCollider);
         Destroy(this.gameObject);
         return;
      }

      /* OLD SIZE INCREASE FORMULA
      // Increase in size initially
      if (timeSinceCreation < SIZE_INCREASE_DURATION) {
         float targetScale = timeSinceCreation / SIZE_INCREASE_DURATION;
        _row.transform.localScale = new Vector3(targetScale, targetScale, 1f);
      }*/

      // Increase in size initially
      if (_row.transform.localScale.x < 1) {
         float currentScale = _row.transform.localScale.x;
         currentScale += Time.deltaTime * SIZE_INCREASE_SPEED;
         _row.transform.localScale = new Vector3(currentScale, currentScale, 1f);
      }

      // Continually decrease our alpha
      Util.setAlpha(text, 1f - (timeSinceCreation / TEXT_LIFETIME) + .4f);
      // Util.setAlpha(iconImage, text.color.a);
   }

   public void setDamageAmount (int damageAmount, bool wasCritical, bool wasBlocked) {
      text.text = "" + damageAmount;

      // Increase the font size if it was a critical hit
      if (wasBlocked) {
         text.fontSize = (int) (text.fontSize * .75f);
      } else if (wasCritical) {
         text.fontSize = (int) (text.fontSize * 1.25f);
      }
   }

   public void customizeForAction (AttackAction action) {
      BasicAbilityData ability = AbilityManager.getAbility(action.abilityGlobalID, AbilityType.Undefined);
      Element element = ability.elementType;

      customizeForAction(element, action.wasCritical, action.damageMagnitude);
   }

   public void customizeForAction (Element element, bool wasCritical, DamageMagnitude magnitude) {
      // Gradient gradient = text.GetComponent<Gradient>();
      text.font = Resources.Load<Font>("Fonts/");
      string fontString = "PhysicalDamage";

      switch (element) {
         case Element.Poison:
            // gradient.vertex2 = Color.magenta;
            fontString = wasCritical ? "PoisonCrit" : "PoisonDamage";
            break;
         case Element.Air:
            // gradient.vertex2 = Color.magenta;
            fontString = wasCritical ? "AirCrit" : "AirDamage";
            break;
         case Element.Earth:
            // gradient.vertex1 = Util.getColor(245, 117, 88);
            // gradient.vertex2 = Util.getColor(140, 70, 60);
            fontString = wasCritical ? "EarthCrit" : "EarthDamage";
            break;
         case Element.Fire:
            // gradient.vertex2 = Color.red;
            fontString = wasCritical ? "FireCrit" : "FireDamage";
            break;
         case Element.Water:
            // gradient.vertex2 = Color.blue;
            fontString = wasCritical ? "WaterCrit" : "WaterDamage";
            break;
         case Element.Physical:
            // gradient.vertex2 = Color.yellow;
            fontString = wasCritical ? "PhysicalCrit" : "PhysicalDamage";
            break;
         case Element.Heal:
            fontString = wasCritical ? "HealCrit" : "HealDamage";
            break;
      }

      // Update the font
      text.font = Resources.Load<Font>("Fonts/" + fontString);

      if (magnitude == DamageMagnitude.Weakness) {
         text.transform.localScale = new Vector3(1.25f, 1.25f, 1.25f);
      } else if (magnitude == DamageMagnitude.Resistant) {
         text.transform.localScale = new Vector3(.8f, .8f, .8f);
      } else {
         text.transform.localScale = new Vector3(1, 1, 1);
      }

      // Update the icon image based on the Elemental damage type
      // iconImage.sprite = Resources.Load<Sprite>("Icons/" + element);
   }

   #region Private Variables

   // The time at which we were created
   protected float _creationTime;

   // The row that contains the text and image
   protected HorizontalLayoutGroup _row;

   // The UI collider instance that we created
   protected GameObject _uiCollider;

   #endregion
}
