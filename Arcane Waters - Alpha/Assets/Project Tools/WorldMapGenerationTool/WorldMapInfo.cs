﻿[System.Serializable]
public class WorldMapInfo
{
   #region Public Variables

   // Number of sectors
   public int sectors;

   // Number of columns
   public int columns;

   // Number of rows
   public int rows;

   #endregion
}
