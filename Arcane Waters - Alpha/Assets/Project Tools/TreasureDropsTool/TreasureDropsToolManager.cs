﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Mirror;
using System.IO;
using System.Xml.Serialization;
using System.Text;
using System.Xml;
using System.Linq;
using System;

public class TreasureDropsToolManager : XmlDataToolManager {
   #region Public Variables

   // Cached drops list
   public Dictionary<int, LootGroupData> treasureDropsCollection = new Dictionary<int, LootGroupData>();

   // Reference to self
   public static TreasureDropsToolManager instance;

   // Reference to the tool scene
   public TreasureDropsToolScene toolScene;

   #endregion

   protected override void Awake () {
      base.Awake();
      instance = this;
      self = this;
   }

   private void Start () {
      // Initialize equipment data first
      Invoke("initializeData", MasterToolScene.loadDelay);
      XmlLoadingPanel.self.startLoading();
      CraftingManager.self.initializeDataCache();
   }

   private void initializeData () {
      EquipmentXMLManager.self.finishedDataSetup.AddListener(() => {
         loadAllDataFiles();
      });

      fetchRecipe();
      EquipmentXMLManager.self.initializeDataCache();
   }

   public void saveDataFile (int xmlId, Biome.Type biomeType, LootGroupData lootGroupData) {
      XmlLoadingPanel.self.startLoading();

      XmlSerializer ser = new XmlSerializer(lootGroupData.GetType());
      var sb = new StringBuilder();
      using (var writer = XmlWriter.Create(sb)) {
         ser.Serialize(writer, lootGroupData);
      }

      string longString = sb.ToString();
      UnityThreadHelper.BackgroundDispatcher.Dispatch(() => {
         DB_Main.updateBiomeTreasureDrops(xmlId, longString, biomeType);

         UnityThreadHelper.UnityDispatcher.Dispatch(() => {
            loadAllDataFiles();
         });
      });
   }

   public void duplicateData (LootGroupData lootData) {
      lootData.lootGroupName = "(Undefined)";

      XmlLoadingPanel.self.startLoading();
      XmlSerializer ser = new XmlSerializer(lootData.GetType());
      var sb = new StringBuilder();
      using (var writer = XmlWriter.Create(sb)) {
         ser.Serialize(writer, lootData);
      }
      string longString = sb.ToString();
      UnityThreadHelper.BackgroundDispatcher.Dispatch(() => {
         DB_Main.updateBiomeTreasureDrops(-1, longString, Biome.Type.None);
         UnityThreadHelper.UnityDispatcher.Dispatch(() => {
            loadAllDataFiles();
         });
      });
   }

   public void loadAllDataFiles () {
      XmlLoadingPanel.self.startLoading();
      treasureDropsCollection = new Dictionary<int, LootGroupData>();

      UnityThreadHelper.BackgroundDispatcher.Dispatch(() => {
         List<XMLPair> xmlPairList = DB_Main.getBiomeTreasureDrops();
         UnityThreadHelper.UnityDispatcher.Dispatch(() => {
            foreach (XMLPair xmlPair in xmlPairList) {
               TextAsset newTextAsset = new TextAsset(xmlPair.rawXmlData);
               LootGroupData treasureCollectionData = Util.xmlLoad<LootGroupData>(newTextAsset);
               int uniqueId = xmlPair.xmlId;

               if (!treasureDropsCollection.ContainsKey(uniqueId)) {
                  treasureDropsCollection.Add(uniqueId, treasureCollectionData);
               }
            }

            toolScene.cacheDatabaseContents(treasureDropsCollection);
            XmlLoadingPanel.self.finishLoading();
         });
      });
   }

   #region Private Variables

   #endregion
}
