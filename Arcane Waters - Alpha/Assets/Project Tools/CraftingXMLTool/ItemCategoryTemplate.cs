﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Mirror;

public class ItemCategoryTemplate : MonoBehaviour {
   #region Public Variables

   // Defines the current category is being selected
   public Item.Category itemCategory;

   // The name of the category
   public Text itemCategoryText;

   // The index of the category
   public Text itemIndexText;

   // Select the category
   public Button selectButton;

   #endregion

   #region Private Variables

   #endregion
}
