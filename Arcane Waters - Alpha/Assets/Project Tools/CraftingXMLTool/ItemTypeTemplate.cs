﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Mirror;
using UnityEngine.Events;

public class ItemTypeTemplate : MonoBehaviour {
   #region Public Variables

   // Displays the type of item
   public Text itemTypeText;

   // Displays the index of item
   public Text itemIndexText;

   // Button for selecting the item
   public Button selectButton;

   // Holds the icon of the item
   public Image spriteIcon;

   // Button for previewing the item
   public Button previewButton;

   #endregion

   #region Private Variables

   #endregion
}
