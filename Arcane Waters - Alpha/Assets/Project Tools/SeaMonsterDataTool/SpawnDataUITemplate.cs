﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Mirror;

public class SpawnDataUITemplate : MonoBehaviour {
   #region Public Variables

   // To select the template
   public Button selectButton;

   // Toggles the UI display
   public Toggle toggleButton;

   // Label of the template
   public Text templateName;

   #endregion

   #region Private Variables
      
   #endregion
}
