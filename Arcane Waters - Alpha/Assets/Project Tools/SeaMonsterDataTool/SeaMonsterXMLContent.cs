﻿public class SeaMonsterXMLContent
{
   // Id of the xml entry
   public int xmlId;

   // Data of the sea monster content
   public SeaMonsterEntityData seaMonsterData;

   // Determines if the entry is enabled in the database
   public bool isEnabled;
}
