﻿using UnityEngine;

namespace MapCreationTool.IssueResolving
{
   public class WrongLayerIssueConfig : MonoBehaviour
   {
      public string fromLayer;
      public string toLayer;
      public int fromSublayer;
      public int toSublayer;
   }
}
