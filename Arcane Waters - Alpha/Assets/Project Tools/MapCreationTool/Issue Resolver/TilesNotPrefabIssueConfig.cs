﻿using UnityEngine;

namespace MapCreationTool.IssueResolving
{
   public class TilesNotPrefabIssueConfig : MonoBehaviour
   {
      public string layer;
      public int sublayer;
      public GameObject prefab;
      public Vector2 prefabOffset;
   }
}