﻿using UnityEngine;

namespace MapCreationTool
{
   public class PrefabConfig : MonoBehaviour
   {
      public GameObject prefab;
      public bool inUnityEditorOnly;
   }
}
