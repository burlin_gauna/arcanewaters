﻿using MapCreationTool.Serialization;

namespace MapCreationTool
{
   public interface IPrefabDataListener
   {
      void dataFieldChanged (DataField field);
   }
}
