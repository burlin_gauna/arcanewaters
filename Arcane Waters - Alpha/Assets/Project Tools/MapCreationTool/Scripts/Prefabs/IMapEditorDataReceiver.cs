﻿namespace MapCreationTool.Serialization
{
   public interface IMapEditorDataReceiver
   {
      void receiveData (DataField[] dataFields);
   }
}
