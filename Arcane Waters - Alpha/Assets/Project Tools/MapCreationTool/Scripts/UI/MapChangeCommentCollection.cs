﻿[System.Serializable]
public class MapChangeCommentCollection
{
   #region Public Variables

   // Array of comments that are in the collection
   public MapChangeComment[] records;

   #endregion

   #region Private Variables

   #endregion
}
