﻿public struct PublishedVersionChange
{
   #region Public Variables

   // The map id of the map that was changed
   public int mapId;

   // The previous published map version of the map
   public int previousVersion;

   // The current published map version of the map
   public int currentVersion;

   #endregion

   #region Private Variables

   #endregion
}
