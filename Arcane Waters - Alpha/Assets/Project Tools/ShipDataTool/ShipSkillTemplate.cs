﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Mirror;

public class ShipSkillTemplate : MonoBehaviour {
   #region Public Variables

   // Delete skill template
   public Button deleteButton;

   // Select this template
   public Button selectButton;

   // Name of the skill to add
   public Text skillNameText;

   // ID of the skill to add
   public Text skillIdText;

   #endregion

   #region Private Variables

   #endregion
}
